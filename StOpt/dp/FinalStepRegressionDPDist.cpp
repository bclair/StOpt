#ifdef USE_MPI
// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <functional>
#include <memory>
#include <boost/mpi.hpp>
#include <Eigen/Dense>
#include "StOpt/core/parallelism/ParallelComputeGridSplitting.h"
#include "StOpt/core/utils/primeNumber.h"
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/core/grids/GridIterator.h"
#include "StOpt/dp/FinalStepRegressionDPDist.h"


using namespace StOpt;
using namespace Eigen;
using namespace std;


FinalStepRegressionDPDist::FinalStepRegressionDPDist(const  shared_ptr<FullGrid> &p_pGridCurrent, const int &p_nbRegime,
        const Eigen::Array< bool, Eigen::Dynamic, 1>   &p_bdimToSplit): m_pGridCurrent(p_pGridCurrent),
    m_nDim(p_pGridCurrent->getDimension()),
    m_nbRegime(p_nbRegime)
{
    boost::mpi::communicator world;
    // initial dimension
    ArrayXi initialDimension   = p_pGridCurrent->getDimensions();
    // organize the hypercube splitting for parallel
    ArrayXi splittingRatio = paraOptimalSplitting(initialDimension, p_bdimToSplit);
    // grid treated by current processor
    m_gridCurrentProc = m_pGridCurrent->getSubGrid(paraSplitComputationGridsProc(initialDimension, splittingRatio, world.rank()));
}

vector<shared_ptr< ArrayXXd > >  FinalStepRegressionDPDist::operator()(const function<double(const int &, const ArrayXd &, const ArrayXd &)>     &p_funcValue,
        const ArrayXXd &p_particles) const
{
    vector<shared_ptr< ArrayXXd > > finalValues(m_nbRegime);
    if (m_gridCurrentProc->getNbPoints() > 0)
    {
        for (int iReg = 0; iReg < m_nbRegime; ++iReg)
            finalValues[iReg] = make_shared<ArrayXXd>(p_particles.cols(), m_gridCurrentProc->getNbPoints());
        shared_ptr<GridIterator>  iterGrid =  m_gridCurrentProc->getGridIterator();
        while (iterGrid->isValid())
        {
            ArrayXd pointCoord = iterGrid->getCoordinate();
            for (int iReg = 0; iReg < m_nbRegime; ++iReg)
                for (int is = 0; is < p_particles.cols(); ++is)
                {
                    (*finalValues[iReg])(is, iterGrid->getCount()) = p_funcValue(iReg, pointCoord, p_particles.col(is));

                }
            iterGrid->next();
        }
    }
    else
    {
        for (int iReg = 0; iReg < m_nbRegime; ++iReg)
            finalValues[iReg] = make_shared<ArrayXXd>();
    }
    return finalValues;
}
#endif


// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef OPTIMIZERBASEINTERP_H
#define OPTIMIZERBASEINTERP_H
#include <Eigen/Dense>
#include "StOpt/core/utils/StateWithStocks.h"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/regression/ContinuationValue.h"
#include "StOpt/regression/GridAndRegressedValue.h"
#include "StOpt/dp/OptimizerBase.h"

/** \file OptimizerBaseInterp.h
 *  \brief Define an abstract class for Dynamic Programming problems solved by Monte Carlo methods  with interpolation for Bellman values
 *     \author Xavier Warin
 */

namespace StOpt
{

/// \class OptimizerBaseInterp OptimizerBaseInterp.h
///  Base class for optimizer for Dynamic Programming  with interpolation for Bellman values  with and without regression methods
class OptimizerBaseInterp : public OptimizerBase
{


public :

    OptimizerBaseInterp() {}

    virtual ~OptimizerBaseInterp() {}


    /// \brief Defines a step in simulation using interpolation in controls
    /// \param p_grid          grid at arrival step after command
    /// \param p_control       defines the controls
    /// \param p_state         defines the state value (modified)
    /// \param p_phiInOut      defines the value function (modified): size number of functions to follow
    virtual void stepSimulateControl(const std::shared_ptr< StOpt::SpaceGrid>   &p_grid, const std::vector< StOpt::GridAndRegressedValue  > &p_control,
                                     StOpt::StateWithStocks &p_state,
                                     Eigen::Ref<Eigen::ArrayXd> p_phiInOut) const = 0 ;


};
}
#endif /* OPTIMIZERBASEINTERP_H */

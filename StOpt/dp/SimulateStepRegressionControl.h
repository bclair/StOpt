// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SIMULATESTEPREGRESSIONCONTROL_H
#define SIMULATESTEPREGRESSIONCONTROL_H
#ifdef OMP
#include <omp.h>
#endif
#include <boost/lexical_cast.hpp>
#include "geners/BinaryFileArchive.hh"
#include "geners/Reference.hh"
#include "StOpt/dp/SimulateStepRegressionBase.h"
#include "StOpt/core/utils/StateWithStocks.h"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/dp/OptimizerBaseInterp.h"
#include "StOpt/regression/GridAndRegressedValue.h"
#include "StOpt/regression/GridAndRegressedValueGeners.h"

/** \file SimulateStepRegressionControl.h
 *  \brief  In simulation part, permits to  use the optimal control stored in in optimization step to calculate de Monte Carlo optimal trajectory
 *  \author Xavier Warin
 */
namespace StOpt
{

/// \class SimulateStepRegressionControl SimulateStepRegressionControl.h
/// One step in forward simulation
class SimulateStepRegressionControl : public SimulateStepRegressionBase
{
private :

    std::shared_ptr<SpaceGrid>  m_pGridFollowing ; ///< global grid at following time step
    std::shared_ptr<StOpt::OptimizerBaseInterp >          m_pOptimize ; ///< optimizer solving the problem for one point and one step
    std::vector< GridAndRegressedValue >  m_control ; ///< to store the optimal control calculated in optimization

public :

    /// \brief default
    SimulateStepRegressionControl() {}
    virtual ~SimulateStepRegressionControl() {}

    /// \brief Constructor
    /// \param p_ar               Archive where continuation values are stored
    /// \param p_iStep            Step number identifier
    /// \param p_nameCont         Name use to store continuation valuation
    /// \param p_pGridFollowing   grid at following time step
    /// \param p_pOptimize        Optimize object defining the transition step
    SimulateStepRegressionControl(gs::BinaryFileArchive &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                  const   std::shared_ptr<SpaceGrid> &p_pGridFollowing,
                                  const  std::shared_ptr<StOpt::OptimizerBaseInterp > &p_pOptimize);

    /// \brief Define one step arbitraging between possible commands
    /// \param p_statevector    Vector of states (regime, stock descriptor, uncertainty)
    /// \param p_phiInOut       actual contract value modified at current time step by applying an optimal command (size : number of function to follow by number of simulations)
    void oneStep(std::vector<StateWithStocks > &p_statevector, Eigen::ArrayXXd  &p_phiInOut) const;

};
}
#endif /* SIMULATESTEPREGRESSIONCONTROL_H */

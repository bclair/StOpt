// Copyright (C) 2016, 2017 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALREGRESSION_H
#define LOCALREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <Eigen/Dense>
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/regression/meshCalculationLocalRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalRegression.h
 *  \brief  Base class for local regressions with adapted mesh size.
 *          See for the linear version the  article "Monte-Carlo valorisation of American options: facts and new algorithms to improve existing methods"
 *          by Bouchard, Warin in "Numerical methods in finance", Springer,2012
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup Local Piecewise regression
 * \brief It implements local local regression
  *@{
 */
/// \class LocalRegression LocalRegression.h
/// To be used in Monte Carlo methods  regression on each cell which is constructed such that each cell has
/// roughly the same number of particles
class LocalRegression : public BaseRegression
{
protected :

    Eigen::ArrayXi  m_nbMesh ;    ///< Number of discretization meshes in each direction: copy because of python binding
    int m_nbMeshTotal ; ///< Total number of meshes
    Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic > m_mesh ;  ///< In each dimension, for each mesh, give the coordinates min and max of the mesh.
    std::vector< std::shared_ptr <Eigen::ArrayXd>  > m_mesh1D ; ///< Mesh representation per dimension
    Eigen::ArrayXi m_simToCell;              ///< For each simulation gives its global mesh number
    std::vector<  std::shared_ptr< std::vector< int> > > m_simulBelongingToCell;  ///< Utility  : to each cell defines the particles lying inside

    /// \brief To a particle affect to cell number
    /// \param p_oneParticle  One point
    /// \return cell number
    int  particleToMesh(const Eigen::ArrayXd &p_oneParticle) const;

public :

    /// \brief Default ructor
    LocalRegression() {}

    /// \brief Constructor
    /// \param  p_nbMesh       discretization in each direction
    /// \param  p_bRotationAndRecale do we use SVD
    LocalRegression(const Eigen::ArrayXi   &p_nbMesh, const bool &p_bRotationAndRecale);

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate          first date is 0?
    /// \param  p_particles          particles used for the meshes.
    ///                              First dimension  : dimension of the problem,
    ///                              second dimension : the  number of particles
    /// \param  p_nbMesh             discretization in each direction
    /// \param  p_bRotationAndRecale do we use SVD
    LocalRegression(const bool &p_bZeroDate,
                    const Eigen::ArrayXXd  &p_particles,
                    const Eigen::ArrayXi   &p_nbMesh,
                    const bool &p_bRotationAndRecale);

    /// \brief Second constructor , only to be used in simulation
    LocalRegression(const   bool &p_bZeroDate,
                    const   std::vector< std::shared_ptr< Eigen::ArrayXd > > &p_mesh1D,
                    const   Eigen::ArrayXd &p_meanX,
                    const   Eigen::ArrayXd   &p_etypX,
                    const   Eigen::MatrixXd   &p_svdMatrix,
                    const   bool &p_bRotationAndRecale) ;

    /// \brief Copy constructor
    /// \param p_object object to copy
    LocalRegression(const LocalRegression   &p_object);

    /// \brief Get some local accessors
    ///@{
    inline const Eigen::ArrayXi &getNbMesh() const
    {
        return m_nbMesh;
    }
    inline const Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic > &getMesh() const
    {
        return m_mesh;
    }
    inline const std::vector< std::shared_ptr< Eigen::ArrayXd > > &getMesh1D() const
    {
        return m_mesh1D;
    }
    inline const Eigen::ArrayXi &getSimToCell() const
    {
        return m_simToCell;
    }
    ///@}

    /// \brief Get some local accessors with copy (useful for python)
    ///@{
    inline Eigen::ArrayXi getNbMeshCopy() const
    {
        return m_nbMesh;
    }
    inline Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic > getMeshCopy() const
    {
        return m_mesh;
    }
    inline std::vector< std::shared_ptr< Eigen::ArrayXd > > getMesh1DCopy() const
    {
        return m_mesh1D;
    }
    inline  Eigen::ArrayXi getSimToCellCopy() const
    {
        return m_simToCell;
    }
    ///@}

    /// \brief Get dimension of the problem
    inline int getDimension() const
    {
        return  m_mesh1D.size();
    }

    /// \brief get the total number of meshes
    inline int getNbMeshTotal() const
    {
        return m_nbMeshTotal;
    }

    /// \brief get the total number of cells
    inline int getNumberOfCells() const
    {
        if (m_nbMesh.size() > 0)
            return m_nbMesh.prod();
        return 1;
    };

    /// \brief To a particle get back the cell it belongs to
    /// \param p_isim particle number
    /// \return cell number it belongs to
    inline int getCellAssociatedToSim(const  int &p_isim) const
    {
        if (m_simToCell.size() > 0)
            return  m_simToCell(p_isim);
        else
            return 0;
    }

    /// \brief calculate a vector of vector of points giving for each cell the vector of points belonging to this cell
    void  evaluateSimulBelongingToCell();

    /// \brief get particles belonging all mesh
    inline  const std::vector<  std::shared_ptr< std::vector< int> > >    &getSimulBelongingToCell() const
    {
        return m_simulBelongingToCell;
    }

    /// \brief to a particle gives its cell number
    /// \param p_oneParticle  One point
    inline int getMeshNumberAssociatedTo(const Eigen::ArrayXd &p_oneParticle) const
    {
        if (((m_nbMesh.size() > 0) && (!m_bZeroDate)) && (p_oneParticle.size() != 0))
        {
            // rotation
            Eigen::VectorXd x = p_oneParticle.matrix();
            x = ((x.array() - m_meanX) / m_etypX).matrix();
            x = m_svdMatrix * x;
            return particleToMesh(x.array());
        }
        else
            return 0;
    }

    /// \brief conditional expectation basis function coefficient calculation for a special cell
    /// \param  p_iCell     cell number
    /// \param  p_fToRegress  function to regress associated to each simulation used in optimization and corresponding to the cell
    /// \return regression coordinates on the basis  (size : the dimension of the problem plus one)
    /// @{
    virtual Eigen::ArrayXd getCoordBasisFunctionOneCell(const int &p_iCell, const Eigen::ArrayXd &p_fToRegress) const  = 0;
    virtual Eigen::ArrayXXd getCoordBasisFunctionMultipleOneCell(const int &p_iCell, const Eigen::ArrayXXd &p_fToRegress) const = 0 ;
    ///@}

    /// \brief Given a particle and the coordinates of the mesh it belongs to, get back the conditional expectation
    /// \param p_oneParticle  The particle generated
    /// \param p_cell      the cell it belongs to
    /// \param p_foncBasisCoef  function basis on the current cell (the row is the number of reconstruction to achieve, the columns the number of function basis)
    /// \return send back the array of conditional expectations
    virtual Eigen::ArrayXd getValuesOneCell(const Eigen::ArrayXd &p_oneParticle, const int &p_cell, const Eigen::ArrayXXd   &p_foncBasisCoef) const = 0;

};
/**@}*/
}
#endif /*LOCALREGRESSION_H*/

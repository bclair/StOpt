// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <vector>
#include <array>
#include <memory>
#include <algorithm>
#include <Eigen/Dense>
#include "StOpt/regression/meshToCoordinates.h"

using namespace std;
using namespace Eigen ;

namespace StOpt
{

void meshCalculationLocalRegression(const ArrayXXd &p_particles, const ArrayXi   &p_nbMesh,
                                    ArrayXi &p_simToCell, Array< array< double, 2>, Dynamic, Dynamic > &p_mesh,
                                    vector< shared_ptr< ArrayXd > > &p_mesh1D)
{
    int nDim =  p_nbMesh.size();
    assert(p_particles.rows() == nDim);

    // number of meshes
    int nbMeshGlob  =  p_nbMesh.prod();
    p_mesh.resize(nDim, nbMeshGlob);
    p_mesh1D.resize(p_nbMesh.size());
    for (int id = 0; id < nDim; ++id)
        p_mesh1D[id] = make_shared< ArrayXd >(p_nbMesh(id) + 1);
    // number of simulations
    int nbSimul = p_particles.cols();
    // utilitary for position in meshes
    int idecCell = 1 ;
    p_simToCell.setConstant(0) ;
    // calculate the meshing (cost linear in simulation number)
    for (int id = 0 ; id < nDim ; ++id)
    {
        vector<double> partDim(nbSimul);
        for (int ip = 0; ip < nbSimul; ++ip)
            partDim[ip] = p_particles(id, ip);
        vector<double>::iterator startD = partDim.begin();
        vector<double>::iterator endD = partDim.end();
        // number of particles per mesh
        int nppm = nbSimul / p_nbMesh(id);
        // partial sort only
        nth_element(startD, startD, endD);
        (*p_mesh1D[id])(0) = partDim[0];
        nth_element(startD + 1, startD + nppm - 1, endD);
        (*p_mesh1D[id])(1) = partDim[nppm - 1];
        for (int j = 1 ; j < p_nbMesh(id) - 1 ; ++j)
        {
            assert((*p_mesh1D[id])(j) >= (*p_mesh1D[id])(j - 1));
            nth_element(startD + j * nppm, startD + (j + 1)*nppm - 1, endD);
            (*p_mesh1D[id])(j + 1) = partDim[(j + 1) * nppm - 1];
        }
        nth_element(startD + (p_nbMesh(id) - 1)*nppm, endD - 1, endD);
        (*p_mesh1D[id])(p_nbMesh(id)) = partDim[nbSimul - 1];
        assert((*p_mesh1D[id])(p_nbMesh(id))  >= (*p_mesh1D[id])(p_nbMesh(id) - 1));
        for (int is = 0 ; is < nbSimul ; ++is)
        {
            int im = 0 ;
            while (p_particles(id, is) > (*p_mesh1D[id])(im + 1)) im++;
            p_simToCell(is) += idecCell * im;
        }
        // offset
        idecCell *= p_nbMesh(id);
    }
    // Utilitary for coordinates
    VectorXi  coord(nDim) ;
    // utilitary
    int nDivInit = ((nDim > 1) ? p_nbMesh.head(nDim - 1).prod() : 1) ;
    // for each mesh  calculate its borders
    for (int im = 0 ; im < nbMeshGlob ; ++im)
    {
        ArrayXi coord = meshToCoordinates(p_nbMesh, nDivInit, im);
        for (int j = 0 ; j < nDim ; ++j)
        {
            // minimal value
            p_mesh(j, im)[0] = (*p_mesh1D[j])(coord(j)) ;
            // maximal value
            p_mesh(j, im)[1] = (*p_mesh1D[j])(coord(j) + 1) ;
        }
    }
}

}

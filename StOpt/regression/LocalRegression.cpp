// Copyright (C) 2017 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include "StOpt/regression/LocalRegression.h"

using namespace std;
using namespace Eigen;

namespace StOpt
{
LocalRegression::LocalRegression(const ArrayXi   &p_nbMesh,
                                 const bool &p_bRotationAndRecale):
    BaseRegression(false, p_bRotationAndRecale),
    m_nbMesh(p_nbMesh),
    m_nbMeshTotal(m_nbMesh.prod()) {}

LocalRegression::LocalRegression(const bool &p_bZeroDate,
                                 const ArrayXXd  &p_particles,
                                 const ArrayXi   &p_nbMesh,
                                 const bool &p_bRotationAndRecale):
    BaseRegression(p_bZeroDate, p_particles, p_bRotationAndRecale),
    m_nbMesh(p_nbMesh),
    m_nbMeshTotal(m_nbMesh.prod()),
    m_simToCell(p_particles.cols())
{
    if ((!m_bZeroDate) && (p_nbMesh.size() != 0))
    {
        meshCalculationLocalRegression(m_particles, m_nbMesh, m_simToCell, m_mesh, m_mesh1D);
    }
    else
    {
        m_simToCell.setConstant(0);
        m_nbMeshTotal = 1;
    }
}

LocalRegression::LocalRegression(const bool &p_bZeroDate, const vector< shared_ptr< ArrayXd > > &p_mesh1D, const   ArrayXd &p_meanX,
                                 const   ArrayXd   &p_etypX, const   MatrixXd   &p_svdMatrix, const  bool &p_bRotationAndRecale) :
    BaseRegression(p_bZeroDate, p_meanX,  p_etypX,  p_svdMatrix, p_bRotationAndRecale)
{
    if ((!m_bZeroDate) && (p_mesh1D.size() > 0))
    {
        m_nbMesh.resize(p_mesh1D.size());
        for (size_t i = 0; i < p_mesh1D.size(); ++i)
            m_nbMesh(i) = p_mesh1D[i]->size() - 1;
        m_nbMeshTotal = m_nbMesh.prod();
        m_mesh1D = p_mesh1D;

        if (!p_bRotationAndRecale)
        {
            m_meanX = ArrayXd::Zero(p_mesh1D.size());
            m_etypX = ArrayXd::Constant(p_mesh1D.size(), 1.);
            m_svdMatrix = MatrixXd::Identity(p_mesh1D.size(), p_mesh1D.size());
        }
    }
    else
        m_nbMeshTotal = 1;
}

LocalRegression::LocalRegression(const LocalRegression   &p_object) : BaseRegression(p_object), m_nbMesh(p_object.getNbMesh()),
    m_nbMeshTotal(p_object.getNbMeshTotal()), m_mesh(p_object.getMesh()),
    m_simToCell(p_object.getSimToCell())
{
    const vector< shared_ptr< ArrayXd > > &mesh1D = p_object.getMesh1D();
    m_mesh1D.resize(mesh1D.size());
    for (size_t i = 0 ; i < m_mesh1D.size(); ++i)
        m_mesh1D[i] = make_shared< ArrayXd>(*mesh1D[i]);
}

int LocalRegression:: particleToMesh(const ArrayXd &p_oneParticle)const
{
    int nBase = p_oneParticle.size() + 1;
    int iCell = 0 ;
    int idecCell = 1;
    for (int id = 0 ; id < nBase - 1 ; id++)
    {
        int iMesh = 1 ;
        while ((p_oneParticle(id) > (*m_mesh1D[id])(iMesh)) && (iMesh < m_mesh1D[id]->size() - 1)) iMesh++;
        iCell += (iMesh - 1) * idecCell;
        idecCell *= m_mesh1D[id]->size() - 1;
    }
    return iCell ;
}

void  LocalRegression::evaluateSimulBelongingToCell()
{
    int nbCells = getNumberOfCells();
    m_simulBelongingToCell.resize(nbCells);
    if (m_particles.cols() > 0)
    {
        for (int icell = 0; icell < nbCells; ++icell)
        {
            m_simulBelongingToCell[icell] = make_shared< vector< int> >();
            m_simulBelongingToCell[icell]->reserve(2 * m_particles.cols() / nbCells);
        }
        for (int is = 0; is <  m_simToCell.size(); ++is)
            m_simulBelongingToCell[m_simToCell(is)]->push_back(is);
    }
    else
    {
        m_simulBelongingToCell[0] = make_shared< vector< int> >(1);
        (*m_simulBelongingToCell[0])[0] = 0;
    }
}

}

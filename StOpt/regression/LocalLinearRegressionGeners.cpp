#include <memory>
#include <geners/IOException.hh>
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "StOpt/core/utils/eigenGeners.h"

using namespace StOpt;
using namespace std;

bool LocalLinearRegressionGeners::write(ostream &p_of, const wrapped_base &p_base,
                                        const bool p_dumpId) const
{
    // If necessary, write out the class id
    const bool status = p_dumpId ? wrappedClassId().write(p_of) : true;

    // Write the object data out
    if (status)
    {
        const wrapped_type &w = dynamic_cast<const wrapped_type &>(p_base);
        gs::write_pod(p_of, w.getBZeroDate());
        gs::write_item(p_of, w.getMesh1D());
        gs::write_item(p_of, w.getMeanX());
        gs::write_item(p_of, w.getEtypX());
        gs::write_item(p_of, w.getSvdMatrix());
        gs::write_pod(p_of, w.getBRotationAndRescale());
    }

    // Return "true" on success
    return status && !p_of.fail();
}

LocalLinearRegression *LocalLinearRegressionGeners::read(const gs::ClassId &p_id, istream &p_in) const
{
    // Validate the class id. You might want to implement
    // class versioning here.
    wrappedClassId().ensureSameId(p_id);

    // Read in the object data
    bool bZeroDate = 0;
    gs::read_pod(p_in, &bZeroDate);
    unique_ptr<vector< shared_ptr< Eigen::ArrayXd > > > mesh1D = gs::read_item<  vector< shared_ptr< Eigen::ArrayXd > > >(p_in);
    unique_ptr< Eigen::ArrayXd> meanX = gs::read_item<  Eigen::ArrayXd>(p_in);
    unique_ptr< Eigen::ArrayXd> etypX = gs::read_item<  Eigen::ArrayXd>(p_in);
    unique_ptr< Eigen::MatrixXd> svdMatrix = gs::read_item<  Eigen::MatrixXd>(p_in);
    bool bRotationAndRescale = false;
    gs::read_pod(p_in, &bRotationAndRescale);

    // Check that the stream is in a valid state
    if (p_in.fail()) throw gs::IOReadFailure("In BIO::read: input stream failure");

    // Return the object
    return new LocalLinearRegression(bZeroDate, *mesh1D, *meanX, *etypX, *svdMatrix, bRotationAndRescale);
}

const gs::ClassId &LocalLinearRegressionGeners::wrappedClassId()
{
    static const gs::ClassId wrapId(gs::ClassId::makeId<wrapped_type>());
    return wrapId;
}


// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef CONTINUATIONVALUE_H
#define CONTINUATIONVALUE_H
#include <iostream>
#include <Eigen/Dense>
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/core/grids/SpaceGrid.h"

/** \file ContinuationValue.h
 *  \brief Calculate and store continuation value function
 *  For a grid store continuation function values using its polynomial coefficients
 *  \author Xavier Warin
 */

namespace StOpt
{

/// \class ContinuationValue ContinuationValue.h
/// Permits to calculate, store conditional values on a grid
class ContinuationValue
{

private :
    std::shared_ptr< SpaceGrid >    m_grid ; ///< grid used to define stock points
    std::shared_ptr< BaseRegression >  m_condExp ; ///< conditional expectation
    Eigen::ArrayXXd m_values ; ///< store basis coefficient for each stock  (nb function basis, nb stock points) if date non zero, cash otherwise
    int m_nbSimul ; // simulation  number

public :
    /// \brief Default constructor
    ContinuationValue() {}

    /// \brief Constructor
    /// \param p_grid   grid for stocks
    /// \param p_condExp regressor for conditional expectation
    /// \param p_cash     matrix to store cash  (number of simulations by nb of stocks)
    ContinuationValue(const  std::shared_ptr< SpaceGrid >   &p_grid,
                      const std::shared_ptr< BaseRegression >   &p_condExp,
                      const Eigen::ArrayXXd &p_cash) :
        m_grid(p_grid), m_condExp(p_condExp), m_values(m_condExp->getCoordBasisFunctionMultiple(p_cash.transpose()).transpose()), m_nbSimul(p_cash.rows())
    {}

    /// \brief Constructor
    ContinuationValue(const std::shared_ptr<BaseRegression>   &p_condExp) :
        m_condExp(p_condExp)
    {}

    /// \brief Load another Continuation value object
    ///  Only a partial load of the objects is achieved
    /// \param p_grid   Grid to load
    /// \param p_condExp Condition expectation
    /// \param p_values coefficient polynomials for regression
    virtual void loadForSimulation(const  std::shared_ptr< SpaceGrid > &p_grid,
                                   const std::shared_ptr< BaseRegression >   &p_condExp,
                                   const Eigen::ArrayXXd &p_values)
    {
        m_grid = p_grid;
        m_condExp = p_condExp;
        m_values = p_values ;
    }

    /// \brief As above but only load the condition expectation operator
    void loadCondExpForSimulation(const std::shared_ptr< BaseRegression>   &p_condExp)
    {
        m_condExp = p_condExp;
    }


    /// \brief Get all simulations conditional expectation
    /// \param p_ptOfStock   grid point for interpolation
    /// \return the continuation value associated to each simulation used in optimization
    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_ptOfStock) const
    {
        return m_condExp->reconstruction(m_grid->createInterpolator(p_ptOfStock)->applyVec(m_values));
    }

    /// \brief Same as before but use an interpolator
    Eigen::ArrayXd getAllSimulations(const Interpolator   &p_interpol) const
    {
        return m_condExp->reconstruction(p_interpol.applyVec(m_values));
    }


    /// \brief Get a conditional expectation for a simulation
    /// \param p_isim    simulation number
    /// \param  p_ptOfStock     stock points
    /// \return the continuation value associated to the given simulation used in optimization
    double  getASimulation(const int &p_isim, const Eigen::ArrayXd &p_ptOfStock) const
    {
        return m_condExp->reconstructionASim(p_isim, m_grid->createInterpolator(p_ptOfStock)->applyVec(m_values));
    }


    /// \brief Same as before but use an interpolator
    /// \param p_isim         simulation number
    /// \param p_interpol     interpolator
    /// \return the continuation value associated to the given simulation used in optimization
    double  getASimulation(const int &p_isim, const Interpolator   &p_interpol) const
    {
        return m_condExp->reconstructionASim(p_isim, p_interpol.applyVec(m_values));
    }


    /// \brief Get conditional expectation for one stock and one simulation
    /// \param  p_ptOfStock     stock points
    /// \param  p_coordinates   simulation coordinates
    double getValue(const Eigen::ArrayXd &p_ptOfStock, const Eigen::ArrayXd &p_coordinates) const
    {
        return  m_condExp->getValue(p_coordinates,  m_grid->createInterpolator(p_ptOfStock)->applyVec(m_values));
    }

    /// \brief Get back particles associated to regression
    inline Eigen::ArrayXXd  getParticles() const
    {
        return m_condExp->getParticles();
    }

    //// \brief Get back
    ///@{
    const Eigen::ArrayXXd &getValues()  const
    {
        return m_values;
    }
    std::shared_ptr< SpaceGrid > getGrid() const
    {
        return m_grid;
    }
    std::shared_ptr<BaseRegression > getCondExp()  const
    {
        return m_condExp ;
    }

    inline int getNbSimul() const
    {
        return m_nbSimul ;
    }
    ///@}

};
}
#endif

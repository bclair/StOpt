// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)

/** \file Pybind11StOptGlobal.cpp
 * \brief Mapping optimizer and simulator  classes to python
 * \author Xavier Warin
 */
#include <Eigen/Dense>
#include <memory>
#include <functional>
#include <array>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include "StOpt/dp/SimulatorDPBase.h"
#include "StOpt/dp/OptimizerDPBase.h"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/core/utils/StateWithStocks.h"
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/dp/TransitionStepRegressionDP.h"
#include "StOpt/dp/FinalStepRegressionDP.h"
#include "StOpt/dp/SimulateStepRegression.h"
#include "StOpt/dp/SimulateStepRegressionControl.h"
#include "StOpt/semilagrangien/SemiLagrangEspCond.h"
#ifdef USE_MPI
#include "StOpt/dp/TransitionStepRegressionDPDist.h"
#include "StOpt/dp/FinalStepRegressionDPDist.h"
#include "StOpt/dp/SimulateStepRegressionDist.h"
#include "StOpt/dp/SimulateStepRegressionControlDist.h"
#include "StOpt/core/parallelism/reconstructProc0Mpi.h"
#endif
#include "StOpt/python/BinaryFileArchiveStOpt.h"
#include "StOpt/python/PayOffBase.h"
#include  "StOpt/python/Pybind11VectorAndList.h"

namespace py = pybind11;

//Wrapper for simulator
//**********************

class PySimulatorDPBase : public StOpt::SimulatorDPBase
{
public:

    using StOpt::SimulatorDPBase::SimulatorDPBase;

    Eigen::MatrixXd getParticles() const override
    {
        PYBIND11_OVERLOAD_PURE(Eigen::MatrixXd, StOpt::SimulatorDPBase, getParticles);
    }

    void stepForward() override
    {
        PYBIND11_OVERLOAD_PURE(void,  StOpt::SimulatorDPBase, stepForward) ;
    }
    void stepBackward() override
    {
        PYBIND11_OVERLOAD_PURE(void,  StOpt::SimulatorDPBase, stepBackward);
    }

    Eigen::MatrixXd  stepForwardAndGetParticles() override
    {
        PYBIND11_OVERLOAD_PURE(Eigen::MatrixXd,  StOpt::SimulatorDPBase, stepForwardAndGetParticles);
    }

    Eigen::MatrixXd  stepBackwardAndGetParticles() override
    {
        PYBIND11_OVERLOAD_PURE(Eigen::MatrixXd,  StOpt::SimulatorDPBase, stepBackwardAndGetParticles);
    }

    int getDimension() const override
    {
        PYBIND11_OVERLOAD_PURE(int, StOpt::SimulatorDPBase, getDimension);
    }
    int getNbStep() const  override
    {
        PYBIND11_OVERLOAD_PURE(int, StOpt::SimulatorDPBase, getNbStep);
    }
    double getStep() const  override
    {
        PYBIND11_OVERLOAD_PURE(double, StOpt::SimulatorDPBase, getStep) ;
    }
    int getNbSimul() const  override
    {
        PYBIND11_OVERLOAD_PURE(int, StOpt::SimulatorDPBase, getNbSimul);
    }
    double getCurrentStep() const  override
    {
        PYBIND11_OVERLOAD_PURE(double, StOpt::SimulatorDPBase, getCurrentStep);
    }
    double  getActuStep() const  override
    {
        PYBIND11_OVERLOAD_PURE(double, StOpt::SimulatorDPBase, getActuStep);
    }
    double  getActu() const override
    {
        PYBIND11_OVERLOAD_PURE(double, StOpt::SimulatorDPBase, getActu);
    }
};


///wrapper for optimization object for Optimization Base object for dynamic programming
///*************************************************************************************
class  PyOptimizerDPBase :  public StOpt::OptimizerDPBase
{
    using StOpt::OptimizerDPBase::OptimizerDPBase;

    std::vector< std::array< double, 2> > getCone(const  std::vector<  std::array< double, 2>  > &p_regionByProcessor) const override
    {
        using vecTep = std::vector< std::array< double, 2> >;
        PYBIND11_OVERLOAD_PURE(vecTep, StOpt::OptimizerDPBase, getCone, p_regionByProcessor);
    }

    Eigen::Array< bool, Eigen::Dynamic, 1> getDimensionToSplit() const override
    {
        using boolArray = Eigen::Array< bool, Eigen::Dynamic, 1> ;
        PYBIND11_OVERLOAD_PURE(boolArray, StOpt::OptimizerDPBase, getDimensionToSplit);
    }

    std::pair< Eigen::ArrayXXd, Eigen::ArrayXXd>   stepOptimize(const std::shared_ptr< StOpt::SpaceGrid> &,
            const Eigen::ArrayXd &, const std::vector<StOpt::ContinuationValue> &,
            const std::vector < std::shared_ptr< Eigen::ArrayXXd > > &) const override
    {
        using pairArray = std::pair< Eigen::ArrayXXd, Eigen::ArrayXXd>;
        PYBIND11_OVERLOAD_PURE(pairArray, StOpt::OptimizerDPBase, stepOptimize);
    }

    void stepSimulate(const std::shared_ptr< StOpt::SpaceGrid>   &p_grid, const std::vector< StOpt::GridAndRegressedValue  > &p_continuation,
                      StOpt::StateWithStocks &p_state,
                      Eigen::Ref<Eigen::ArrayXd> p_phiInOut) const override
    {
        PYBIND11_OVERLOAD_PURE(void, StOpt::OptimizerDPBase, stepSimulate, p_grid, p_continuation, p_state, p_phiInOut);
    }

    void stepSimulateControl(const std::shared_ptr< StOpt::SpaceGrid>   &p_grid, const std::vector< StOpt::GridAndRegressedValue  > &p_control,
                             StOpt::StateWithStocks &p_state,
                             Eigen::Ref<Eigen::ArrayXd> p_phiInOut) const override
    {
        PYBIND11_OVERLOAD_PURE(void, StOpt::OptimizerDPBase, stepSimulateControl, p_grid, p_control, p_state, p_phiInOut);
    }

    int getNbRegime() const override
    {
        PYBIND11_OVERLOAD_PURE(int,  StOpt::OptimizerDPBase, getNbRegime);
    }

    int getNbControl() const override
    {
        PYBIND11_OVERLOAD_PURE(int,  StOpt::OptimizerDPBase, getNbControl);
    }

    std::shared_ptr< StOpt::SimulatorDPBase > getSimulator() const override
    {
        //using sharedPtrSim=std::shared_ptr< StOpt::SimulatorDPBase >
        PYBIND11_OVERLOAD_PURE(std::shared_ptr< StOpt::SimulatorDPBase >,  StOpt::OptimizerDPBase, getSimulator);
    }

    int getSimuFuncSize() const  override
    {
        PYBIND11_OVERLOAD_PURE(int, StOpt::OptimizerDPBase, getSimuFuncSize);
    }
};

// wrapper for Final Step
//***********************
class PyFinalStepRegressionDP: StOpt::FinalStepRegressionDP
{
public:

    using StOpt::FinalStepRegressionDP::FinalStepRegressionDP;

    std::vector< Eigen::ArrayXXd >   operator()(py::object &p_funcValue, const Eigen::ArrayXXd &p_particles) const
    {
        auto lambda = [p_funcValue](const int &p_i, const Eigen::ArrayXd & p_x, const Eigen::ArrayXd & p_y)->double { return p_funcValue(p_i, p_x, p_y).cast<double>();};

        std::vector< std::shared_ptr<Eigen::ArrayXXd>  >  resLoc = StOpt::FinalStepRegressionDP::operator()(std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)> (lambda), p_particles);
        std::vector< Eigen::ArrayXXd > res;
        res.reserve(resLoc.size());
        for (const auto   &item : resLoc)
            res.push_back(*item);
        return res;
    }

    std::vector< Eigen::ArrayXXd  >  set(const  std::shared_ptr<StOpt::PayOffBase>   &p_funcValue, const Eigen::ArrayXXd &p_particles) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > > res = StOpt::FinalStepRegressionDP::operator()(p_funcValue->getFunction(), p_particles);
        std::vector<Eigen::ArrayXXd >result;
        result.reserve(res.size());
        for (const auto &item : res)
            result.push_back(*item);
        return result;
    }
};

// wrapper for Transition step
//*****************************

class PyTransitionStepRegressionDP: StOpt::TransitionStepRegressionDP
{
public :
    PyTransitionStepRegressionDP(const std::shared_ptr< StOpt::SpaceGrid >   &p_gridCurrent,  const std::shared_ptr< StOpt::SpaceGrid >   &p_gridPrevious, const  std::shared_ptr< StOpt::OptimizerDPBase>   &p_optimizer):
        StOpt::TransitionStepRegressionDP(std::dynamic_pointer_cast<StOpt::FullGrid>(p_gridCurrent),
                                          std::dynamic_pointer_cast<StOpt::FullGrid>(p_gridPrevious),
                                          p_optimizer) {  }

    py::list  oneStepWrap(const std::vector< Eigen::ArrayXXd >  &p_phiIn,
                          const std::shared_ptr< StOpt::BaseRegression>     &p_condExp) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  input;
        input.reserve(p_phiIn.size());
        for (const auto &item : p_phiIn)
        {
            input.push_back(std::make_shared<Eigen::ArrayXXd>(item));
        }
        std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  Eigen::ArrayXXd > > > retLoc = StOpt::TransitionStepRegressionDP::oneStep(input, p_condExp);
        py::list res ;
        py::list res1;
        for (const auto &item : retLoc.first)
            res1.append(*item);
        py::list res2;
        for (const auto &item : retLoc.second)
            res2.append(*item);
        res.append(res1);
        res.append(res2);
        return res;
    }

    void dumpContinuationValuesWrap(std::shared_ptr<BinaryFileArchiveStOpt>  p_ar, const std::string &p_name, const int &p_iStep, const  py::list  &p_phiIn,  const  py::list   &p_control, const   std::shared_ptr<StOpt::BaseRegression>    &p_condExp) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  phiIn = convertFromListShPtr<Eigen::ArrayXXd>(p_phiIn);
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  control = convertFromListShPtr<Eigen::ArrayXXd>(p_control);
        StOpt::TransitionStepRegressionDP::dumpContinuationValues(p_ar, p_name, p_iStep, phiIn, control, p_condExp);
    }
};


// Wrapper for SimulateStepRegression
class PySimulateStepRegression : StOpt::SimulateStepRegression

{
public :
    PySimulateStepRegression(BinaryFileArchiveStOpt &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                             const   std::shared_ptr< StOpt::SpaceGrid > &p_pGridFollowing,
                             const  std::shared_ptr< StOpt::OptimizerDPBase > &p_pOptimize):
        StOpt::SimulateStepRegression(p_ar, p_iStep, p_nameCont, p_pGridFollowing, p_pOptimize)
    {}

    py::list  oneStepWrap(const py::list    &p_statevector,  const Eigen::ArrayXXd   &p_phiInOut) const
    {
        std::vector<StOpt::StateWithStocks> stateLoc = convertFromList<StOpt::StateWithStocks>(p_statevector);
        Eigen::ArrayXXd phiLoc(p_phiInOut);
        StOpt::SimulateStepRegression::oneStep(stateLoc, phiLoc);
        py::list ret;
        ret.append(py::cast(stateLoc));
        ret.append(py::cast(phiLoc));
        return ret ;
    }
};


// Wrapper for SimulateStepRegressionControl
class PySimulateStepRegressionControl : StOpt::SimulateStepRegressionControl

{
public :
    PySimulateStepRegressionControl(BinaryFileArchiveStOpt &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                    const   std::shared_ptr< StOpt::SpaceGrid > &p_pGridFollowing, const  std::shared_ptr< StOpt::OptimizerBaseInterp > &p_pOptimize):
        StOpt::SimulateStepRegressionControl(p_ar, p_iStep, p_nameCont, p_pGridFollowing, p_pOptimize)
    {}

    py::list  oneStepWrap(const py::list    &p_statevector,  const Eigen::ArrayXXd   &p_phiInOut) const
    {
        std::vector<StOpt::StateWithStocks> stateLoc = convertFromList<StOpt::StateWithStocks>(p_statevector);
        Eigen::ArrayXXd phiLoc(p_phiInOut);
        StOpt::SimulateStepRegressionControl::oneStep(stateLoc, phiLoc);
        py::list ret;
        ret.append(py::cast(stateLoc));
        ret.append(py::cast(phiLoc));
        return ret ;
    }
};



#ifdef USE_MPI

class PyFinalStepRegressionDPDist: StOpt::FinalStepRegressionDPDist
{
public:

    PyFinalStepRegressionDPDist(const  std::shared_ptr<StOpt::SpaceGrid> &p_pGridCurrent, const int &p_nbRegime,
                                const Eigen::Array< bool, Eigen::Dynamic, 1>   &p_bdimToSplit):
        StOpt::FinalStepRegressionDPDist(std::dynamic_pointer_cast<StOpt::FullGrid>(p_pGridCurrent), p_nbRegime, p_bdimToSplit) {}

    std::vector< Eigen::ArrayXXd >   operator()(py::object &p_funcValue, const Eigen::ArrayXXd &p_particles) const
    {
        auto lambda = [p_funcValue](const int &p_i, const Eigen::ArrayXd & p_x, const Eigen::ArrayXd & p_y)->double { return p_funcValue(p_i, p_x, p_y).cast<double>();};

        std::vector< std::shared_ptr<Eigen::ArrayXXd>  >  resLoc = StOpt::FinalStepRegressionDPDist::operator()(std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)> (lambda), p_particles);
        std::vector< Eigen::ArrayXXd > res;
        res.reserve(resLoc.size());
        for (const auto   &item : resLoc)
            res.push_back(*item);
        return res;
    }
    std::vector< Eigen::ArrayXXd  >  set(const  std::shared_ptr<StOpt::PayOffBase>   &p_funcValue, const Eigen::ArrayXXd &p_particles) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > > res = StOpt::FinalStepRegressionDPDist::operator()(p_funcValue->getFunction(), p_particles);
        std::vector<Eigen::ArrayXXd >result;
        result.reserve(res.size());
        for (const auto &item : res)
            result.push_back(*item);
        return result;
    }
};


class PyTransitionStepRegressionDPDist: StOpt::TransitionStepRegressionDPDist
{
public :
    PyTransitionStepRegressionDPDist(const std::shared_ptr< StOpt::SpaceGrid >   &p_gridCurrent,  const std::shared_ptr< StOpt::SpaceGrid >   &p_gridPrevious, const  std::shared_ptr< StOpt::OptimizerDPBase>   &p_optimizer):
        StOpt::TransitionStepRegressionDPDist(std::dynamic_pointer_cast<StOpt::FullGrid>(p_gridCurrent),
                                              std::dynamic_pointer_cast<StOpt::FullGrid>(p_gridPrevious),
                                              p_optimizer) {  }

    py::list  oneStepWrap(const std::vector< Eigen::ArrayXXd >  &p_phiIn,
                          const std::shared_ptr< StOpt::BaseRegression>     &p_condExp) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  input;
        input.reserve(p_phiIn.size());
        for (const auto &item : p_phiIn)
        {
            input.push_back(std::make_shared<Eigen::ArrayXXd>(item));
        }
        std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  Eigen::ArrayXXd > > > retLoc = StOpt::TransitionStepRegressionDPDist::oneStep(input, p_condExp);
        py::list res ;
        py::list res1;
        for (const auto &item : retLoc.first)
            res1.append(*item);
        py::list res2;
        for (const auto &item : retLoc.second)
            res2.append(*item);
        res.append(res1);
        res.append(res2);
        return res;
    }

    void dumpContinuationValuesWrap(std::shared_ptr<BinaryFileArchiveStOpt>  p_ar, const std::string &p_name, const int &p_iStep, const  py::list  &p_phiIn,  const  py::list   &p_control, const   std::shared_ptr<StOpt::BaseRegression>    &p_condExp, const bool &p_bOneFile) const
    {
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  phiIn = convertFromListShPtr<Eigen::ArrayXXd>(p_phiIn);
        std::vector< std::shared_ptr< Eigen::ArrayXXd > >  control = convertFromListShPtr<Eigen::ArrayXXd>(p_control);
        StOpt::TransitionStepRegressionDPDist::dumpContinuationValues(p_ar, p_name, p_iStep, phiIn, control, p_condExp, p_bOneFile);
    }
};


// Wrapper for SimulateStepRegression
class PySimulateStepRegressionDist : StOpt::SimulateStepRegressionDist

{
public :
    PySimulateStepRegressionDist(BinaryFileArchiveStOpt &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                 const   std::shared_ptr< StOpt::SpaceGrid > &p_pGridFollowing,
                                 const  std::shared_ptr< StOpt::OptimizerDPBase > &p_pOptimize,
                                 const bool &p_bOneFile):
        StOpt::SimulateStepRegressionDist(p_ar, p_iStep, p_nameCont, std::dynamic_pointer_cast<StOpt::FullGrid>(p_pGridFollowing), p_pOptimize, p_bOneFile)
    {}

    py::list  oneStepWrap(const py::list    &p_statevector,  const Eigen::ArrayXXd   &p_phiInOut) const
    {
        std::vector<StOpt::StateWithStocks> stateLoc = convertFromList<StOpt::StateWithStocks>(p_statevector);
        Eigen::ArrayXXd phiLoc(p_phiInOut);
        StOpt::SimulateStepRegressionDist::oneStep(stateLoc, phiLoc);
        py::list ret;
        ret.append(py::cast(stateLoc));
        ret.append(py::cast(phiLoc));
        return ret ;
    }
};


// Wrapper for SimulateStepRegressionControl
class PySimulateStepRegressionControlDist : StOpt::SimulateStepRegressionControlDist

{
public :
    PySimulateStepRegressionControlDist(BinaryFileArchiveStOpt &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                        const   std::shared_ptr< StOpt::SpaceGrid > &p_pGridCurrent,
                                        const   std::shared_ptr< StOpt::SpaceGrid > &p_pGridFollowing,
                                        const  std::shared_ptr< StOpt::OptimizerDPBase > &p_pOptimize,
                                        const bool &p_bOneFile):
        StOpt::SimulateStepRegressionControlDist(p_ar, p_iStep, p_nameCont, std::dynamic_pointer_cast<StOpt::FullGrid>(p_pGridCurrent),
                std::dynamic_pointer_cast<StOpt::FullGrid>(p_pGridFollowing), p_pOptimize, p_bOneFile)
    {}

    py::list  oneStepWrap(const py::list    &p_statevector,  const Eigen::ArrayXXd   &p_phiInOut) const
    {
        std::vector<StOpt::StateWithStocks> stateLoc = convertFromList<StOpt::StateWithStocks>(p_statevector);
        Eigen::ArrayXXd phiLoc(p_phiInOut);
        StOpt::SimulateStepRegressionControlDist::oneStep(stateLoc, phiLoc);
        py::list ret;
        ret.append(py::cast(stateLoc));
        ret.append(py::cast(phiLoc));
        return ret ;
    }
};

Eigen::ArrayXd  pyReconstructProc0Mpi(const Eigen::ArrayXd &p_point, const std::shared_ptr< StOpt::SpaceGrid> &p_grid, const Eigen::ArrayXXd    &p_values, const Eigen::Array< bool, Eigen::Dynamic, 1>   &p_bdimToSplit)
{
    std::shared_ptr< Eigen::ArrayXXd > values = std::make_shared<Eigen::ArrayXXd > (p_values);
    return reconstructProc0Mpi(p_point, std::dynamic_pointer_cast<StOpt::FullGrid>(p_grid), values, p_bdimToSplit);
}

#endif



namespace py = pybind11;

/// \brief Encapsulation for regression module
PYBIND11_MODULE(StOptGlobal, m)
{


    py::class_<StOpt::StateWithStocks>(m, "StateWithStocks")
    .def(py::init< const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &>())
    .def("getPtStock", &StOpt::StateWithStocks::getPtStock, py::return_value_policy::copy) // copy instead fo reference
    .def("getRegime", &StOpt::StateWithStocks::getRegime)
    .def("setPtStock", &StOpt::StateWithStocks::setPtStock)
    .def("getStochasticRealization", &StOpt::StateWithStocks::getStochasticRealization,  py::return_value_policy::copy)
    .def("setStochasticRealization", &StOpt::StateWithStocks::setStochasticRealization)
    .def("setRegime", &StOpt::StateWithStocks::setRegime)
    ;


    // map simulator DP base
    //**********************
    py::class_<StOpt::SimulatorDPBase, std::shared_ptr<StOpt::SimulatorDPBase>, PySimulatorDPBase >(m, "SimulatorDPBase")
    .def(py::init<>())
    .def("getParticles", &StOpt::SimulatorDPBase::getParticles)
    .def("stepForward", &StOpt::SimulatorDPBase::stepForward)
    .def("stepBackward", &StOpt::SimulatorDPBase::stepBackward)
    .def("stepForwardAndGetParticles", &StOpt::SimulatorDPBase::stepForwardAndGetParticles)
    .def("stepBackwardAndGetParticles", &StOpt::SimulatorDPBase::stepBackwardAndGetParticles)
    .def("getDimension", &StOpt::SimulatorDPBase::getDimension)
    .def("getNbStep", &StOpt::SimulatorDPBase::getNbStep)
    .def("getStep", &StOpt::SimulatorDPBase::getStep)
    .def("getCurrentStep", &StOpt::SimulatorDPBase::getCurrentStep)
    .def("getNbSimul", &StOpt::SimulatorDPBase::getNbSimul)
    .def("getActuStep", &StOpt::SimulatorDPBase::getActuStep)
    .def("getActu", &StOpt::SimulatorDPBase::getActu)
    ;

    // map optimizer DP base
    //**********************
    py::class_<StOpt::OptimizerDPBase, std::shared_ptr<StOpt::OptimizerDPBase>, PyOptimizerDPBase>(m, "OptimizerDPBase")
    .def("getCone",  [](const StOpt::OptimizerDPBase & p_x, const  std::vector< Eigen::ArrayXd >    &p_region)
    {
        std::vector< std::array< double, 2> > region;
        region.reserve(p_region.size());
        for (const auto &item : p_region)
        {
            region.push_back(std::array< double, 2> {{ item[0], item[1]}});
        }
        std::vector< std::array< double, 2> > resLoc = p_x.getCone(region);
        std::vector< Eigen::ArrayXd > result;
        result.reserve(resLoc.size());
        for (const auto &item : resLoc)
        {
            Eigen::ArrayXd dom(2);
            dom(0) = item[0];
            dom(1) = item[1];
            result.push_back(dom);
        }
        return result;
    }
        )
    .def("getDimensionToSplit",  &StOpt::OptimizerDPBase::getDimensionToSplit)
    .def("stepOptimize", [](const StOpt::OptimizerDPBase & p_x,
                            const std::shared_ptr< StOpt::SpaceGrid> &p_grid,
                            const Eigen::ArrayXd   & p_reg,
                            const std::vector<StOpt::ContinuationValue> &p_cont,
                            const std::vector < Eigen::ArrayXXd  > &p_val)
    {
        std::vector < std::shared_ptr< Eigen::ArrayXXd > > val;
        val.reserve(p_val.size());
        for (const auto &item : p_val)
            val.push_back(std::make_shared<Eigen::ArrayXXd>(item));
        std::pair< Eigen::ArrayXXd, Eigen::ArrayXXd>  resLoc = p_x.stepOptimize(p_grid, p_reg, p_cont, val);
        py::list res ;
        res.append(py::cast(resLoc.first));
        res.append(py::cast(resLoc.second));
        return res;
    }
        )
    .def("stepSimulate", &StOpt::OptimizerDPBase::stepSimulate)
    .def("stepSimulateControl", &StOpt::OptimizerDPBase::stepSimulateControl)
    .def("getNbRegime", &StOpt::OptimizerDPBase::getNbRegime)
    .def("getNbControl", &StOpt::OptimizerDPBase::getNbControl)
    .def("getSimulator", &StOpt::OptimizerDPBase::getSimulator)
    .def("getSimuFuncSize", &StOpt::OptimizerDPBase::getSimuFuncSize)
    ;

    // map final pay off
    //******************
    py::class_<StOpt::PayOffBase, std::shared_ptr<StOpt::PayOffBase>  >(m, "PayOffBase");

    // // maps final step
    py::class_<PyFinalStepRegressionDP, std::shared_ptr<PyFinalStepRegressionDP>  >(m, "FinalStepRegressionDP")
    .def(py::init< const std::shared_ptr<StOpt::SpaceGrid>  &, const int & >())
    .def("set", &PyFinalStepRegressionDP::set)
    .def("_setItem_", &PyFinalStepRegressionDP::operator())
    ;


    /// map one time step transition
    py::class_<PyTransitionStepRegressionDP, std::shared_ptr<PyTransitionStepRegressionDP> >(m, "TransitionStepRegressionDP")
    .def(py::init< const std::shared_ptr<StOpt::SpaceGrid> &,
         const  std::shared_ptr<StOpt::SpaceGrid> &, const std::shared_ptr<StOpt::OptimizerDPBase > &>())
    .def("oneStep", & PyTransitionStepRegressionDP::oneStepWrap)
    .def("dumpContinuationValues", & PyTransitionStepRegressionDP::dumpContinuationValuesWrap)
    ;

    // maps SimulateStepRegression for simulation purpose
    py::class_<PySimulateStepRegression, std::shared_ptr<PySimulateStepRegression> >(m, "SimulateStepRegression")
    .def(py::init< BinaryFileArchiveStOpt &,  const int &,  const std::string &,
         const   std::shared_ptr< StOpt::SpaceGrid > &, const  std::shared_ptr< StOpt::OptimizerDPBase > & >())
    .def("oneStep", &PySimulateStepRegression::oneStepWrap)
    ;


    py::class_<PySimulateStepRegressionControl, std::shared_ptr<PySimulateStepRegressionControl> >(m, "SimulateStepRegressionControl")
    .def(py::init< BinaryFileArchiveStOpt &,  const int &,  const std::string &,
         const   std::shared_ptr< StOpt::SpaceGrid > &, const  std::shared_ptr< StOpt::OptimizerDPBase > & >())
    .def("oneStep", & PySimulateStepRegressionControl::oneStepWrap)
    ;

#ifdef USE_MPI

    py::class_<PyFinalStepRegressionDPDist, std::shared_ptr<PyFinalStepRegressionDPDist>  >(m, "FinalStepRegressionDPDist")
    .def(py::init< const std::shared_ptr<StOpt::SpaceGrid>  &, const int &, const Eigen::Array< bool, Eigen::Dynamic, 1>  & >())
    .def("set", &PyFinalStepRegressionDPDist::set)
    .def("_setItem_", &PyFinalStepRegressionDPDist::operator())
    ;

    py::class_<PyTransitionStepRegressionDPDist, std::shared_ptr<PyTransitionStepRegressionDPDist> >(m, "TransitionStepRegressionDPDist")
    .def(py::init< const std::shared_ptr<StOpt::SpaceGrid> &,
         const  std::shared_ptr<StOpt::SpaceGrid> &, const std::shared_ptr<StOpt::OptimizerDPBase > &>())
    .def("oneStep", & PyTransitionStepRegressionDPDist::oneStepWrap)
    .def("dumpContinuationValues", & PyTransitionStepRegressionDPDist::dumpContinuationValuesWrap)
    ;

    py::class_<PySimulateStepRegressionDist, std::shared_ptr<PySimulateStepRegressionDist> >(m, "SimulateStepRegressionDist")
    .def(py::init< BinaryFileArchiveStOpt &,  const int &,  const std::string &,
         const   std::shared_ptr< StOpt::SpaceGrid > &, const  std::shared_ptr< StOpt::OptimizerDPBase > &, const bool & >())
    .def("oneStep", &PySimulateStepRegressionDist::oneStepWrap)
    ;


    py::class_<PySimulateStepRegressionControlDist, std::shared_ptr<PySimulateStepRegressionControlDist> >(m, "SimulateStepRegressionControlDist")
    .def(py::init< BinaryFileArchiveStOpt &,  const int &,  const std::string &, const   std::shared_ptr< StOpt::SpaceGrid > &,
         const   std::shared_ptr< StOpt::SpaceGrid > &, const  std::shared_ptr< StOpt::OptimizerDPBase > &, const bool & >())
    .def("oneStep", & PySimulateStepRegressionControlDist::oneStepWrap)
    ;

    m.def("reconstructProc0Mpi", & pyReconstructProc0Mpi);

#endif

}

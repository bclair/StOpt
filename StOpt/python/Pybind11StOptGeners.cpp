// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include "geners/StringArchive.hh"
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include "StOpt/python/BinaryFileArchiveStOpt.h"


namespace py = pybind11;


/// \brief Encapsulation for regression module
PYBIND11_MODULE(StOptGeners, m)
{

    /**  \file PyPythonGeners.cpp
       *   \brief Map Geners Archive to python for StOpt library
       *   \author Xavier Warin
       */

    // map geners for special use in StOpt
    //***********************************
    py::class_<BinaryFileArchiveStOpt, std::shared_ptr<BinaryFileArchiveStOpt> >(m, "BinaryFileArchive")
    .def(py::init< const char *, const char * >())
    .def("getNamesForGridAndRegressedValue", &BinaryFileArchiveStOpt::getNamesForGridAndRegressedValue)
    .def("getNamesForContinuationCut", &BinaryFileArchiveStOpt::getNamesForContinuationCut)
    .def("dumpGridAndRegressedValue", &BinaryFileArchiveStOpt::dumpGridAndRegressedValue)
    .def("readGridAndRegressedValue", &BinaryFileArchiveStOpt::readGridAndRegressedValue)
    .def("dumpSome2DArray", &BinaryFileArchiveStOpt::dumpSome2DArray)
    .def("dumpSomeRegressor", &BinaryFileArchiveStOpt::dumpSomeRegressor)
    .def("dumpSomeStringVector", &BinaryFileArchiveStOpt::dumpSomeStringVector)
    .def("readSome2DArray", &BinaryFileArchiveStOpt::readSome2DArray)
    .def("readSomeRegressor", &BinaryFileArchiveStOpt::readSomeRegressor)
    .def("readSomeStringVector", &BinaryFileArchiveStOpt::readSomeStringVector)
#ifdef SDDPPYTHON
    .def("dump", &BinaryFileArchiveStOpt::dumpLocalLinearRegressionForSDDP)
    .def("dump", &BinaryFileArchiveStOpt::dumpLocalVisitedStateForSDDP)
#endif
    ;

    py::class_<gs::StringArchive, std::shared_ptr<gs::StringArchive> > (m, "StringArchive")
    .def(py::init< >())
    ;
}

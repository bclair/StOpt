// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SDDPVISITEDSTATE_H
#define SDDPVISITEDSTATE_H
#include <memory>
#include <Eigen/Dense>
#include "StOpt/regression/LocalRegression.h"


/**  \file SDDPVisitedStates.h
 *   \brief Storing visited states during simulation
 *   \author Xavier Warin
 */
namespace StOpt
{

/// \class SDDPVisitedStates SDDPLocalLinearCut.h
/// Permits to store visited cuts in SDDP forward resolution
class SDDPVisitedStates
{
private :

    std::vector< std::shared_ptr< Eigen::ArrayXd > > m_stateVisited ; ///< vector of  state visited
    std::vector<int>  m_associatedMesh ; /// mesh associated (state visited conditionally)

public :

    /// \brief Default constructor
    SDDPVisitedStates();

    /// \brief Second constructor with all states
    SDDPVisitedStates(const std::vector< std::shared_ptr< Eigen::ArrayXd >  > &p_stateVisited, const std::vector<int> &p_associatedMesh) ;

    /// \brief add a state
    /// \param  p_state state to add
    /// \param  p_particle particle  used for conditional cut
    /// \param  p_regressor     regressor used
    void addVisitedState(const std::shared_ptr< Eigen::ArrayXd > &p_state, const Eigen::ArrayXd &p_particle, const LocalRegression   &p_regressor);

    /// \brief add a state for particles
    /// \param  p_state state to add
    /// \param  p_regressor     regressor used
    void addVisitedStateForAll(const std::shared_ptr< Eigen::ArrayXd > &p_state,  const LocalRegression   &p_regressor);

    ///\brief Some accessor
    //@{
    inline int getStateSize() const
    {
        return m_stateVisited.size();   /// Number of states visited
    }
    inline int getMeshAssociatedToState(const int &p_istate) const
    {
        return  m_associatedMesh[p_istate];
    }
    inline std::shared_ptr<Eigen::ArrayXd > getAState(const int &p_istate) const
    {
        return m_stateVisited[p_istate];
    }
    inline const std::vector< std::shared_ptr< Eigen::ArrayXd > > &getStateVisited() const
    {
        return  m_stateVisited;
    }
    inline const std::vector<int>  &getAssociatedMesh() const
    {
        return m_associatedMesh ;
    }
    //@}

    ///\brief print function for debug
    void print() const;

#ifdef USE_MPI
    /// \brief Send all cut to root
    void sendToRoot();

    /// \brief Send all cuts from root to all processor
    void sendFromRoot();
#endif

};
}
#endif /* SDDPVISITEDSTATES_H */

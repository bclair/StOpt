#ifdef USE_MPI
#include <boost/version.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>
#include "StOpt/core/parallelism/gathervExtension.hpp"
#include <boost/serialization/vector.hpp>
#include "StOpt/core/utils/eigenSerialization.h"
#include "StOpt/core/utils/stdSharedPtrSerialization.h"
#endif
#include <memory>
#include <iostream>
#include "StOpt/sddp/SDDPVisitedStates.h"

using namespace Eigen;
using namespace std;

namespace StOpt
{

SDDPVisitedStates:: SDDPVisitedStates() {}

SDDPVisitedStates:: SDDPVisitedStates(const vector< shared_ptr< Eigen::ArrayXd >  > &p_stateVisited, const vector< int > &p_associatedMesh)   : m_stateVisited(p_stateVisited),
    m_associatedMesh(p_associatedMesh) {}

void SDDPVisitedStates::addVisitedState(const shared_ptr< ArrayXd > &p_state, const ArrayXd &p_particle, const LocalRegression &p_regressor)
{
    int ncell = p_regressor.getMeshNumberAssociatedTo(p_particle);
#ifdef _OPENMP
    #pragma omp critical (visited)
#endif
    {
        m_stateVisited.push_back(p_state);
        m_associatedMesh.push_back(ncell);
    }
}

void SDDPVisitedStates::addVisitedStateForAll(const shared_ptr< ArrayXd > &p_state, const LocalRegression &p_regressor)
{
    int nbCell = p_regressor.getNbMeshTotal();
    for (int icell = 0; icell < nbCell; ++icell)
    {
        m_stateVisited.push_back(p_state);
        m_associatedMesh.push_back(icell);
    }
}

void SDDPVisitedStates::print() const
{
    cout << "States visited " << endl ;
    for (size_t i = 0; i < m_stateVisited.size() ; ++i)
        cout << "For  mesh " << m_associatedMesh[i] << " visited state " << *m_stateVisited[i] << endl ;
}

#ifdef USE_MPI
void SDDPVisitedStates::sendToRoot()
{
    boost::mpi::communicator world;
    vector<int>  meshVisited(m_associatedMesh);
    gatherv(world, meshVisited.data(), meshVisited.size(), m_associatedMesh, 0);
    ArrayXd  state;
    int isizeState = ((m_stateVisited.size() > 0) ? m_stateVisited[0]->size() : 0);
    state.resize(m_stateVisited.size()*isizeState);
    for (size_t i = 0; i <  m_stateVisited.size(); ++i)
        state.segment(i * isizeState, isizeState) = *(m_stateVisited[i]);
    ArrayXd stateGather;
    gatherv(world, state.data(), state.size(), stateGather, 0);
    if (world.rank() == 0)
    {
        int isizeState = m_stateVisited[0]->size();
        m_stateVisited.resize(m_associatedMesh.size());
        for (size_t i = 0; i < m_associatedMesh.size(); ++i)
        {
            m_stateVisited[i] = make_shared<ArrayXd>(stateGather.segment(isizeState * i, isizeState));
        }
    }
}

void SDDPVisitedStates::sendFromRoot()
{
    boost::mpi::communicator world;
    // Add this because Bug on mac (nullify existing  vectors)
    if (world.rank() != 0)
    {
        m_stateVisited.clear();
        m_associatedMesh.clear();
    }
    boost::mpi:: broadcast(world, m_associatedMesh, 0);
    // boost BUG
#if BOOST_VERSION <  105600
    boost::mpi:: broadcast(world, m_stateVisited, 0);
#else
    ArrayXd  state;
    if (world.rank() == 0)
    {
        int isizeState = ((m_stateVisited.size() > 0) ? m_stateVisited[0]->size() : 0);
        state.resize(m_stateVisited.size()*isizeState);
        for (size_t i = 0; i <  m_stateVisited.size(); ++i)
            state.segment(i * isizeState, isizeState) = *(m_stateVisited[i]);
    }
    boost::mpi:: broadcast(world, state, 0);
    if (world.rank() > 0)
    {
        if (state.size() > 0)
        {
            int isizeState = state.size() / m_associatedMesh.size();
            m_stateVisited.resize(m_associatedMesh.size());
            for (size_t i = 0; i < m_associatedMesh.size(); ++i)
            {
                m_stateVisited[i] = make_shared<ArrayXd>(state.segment(isizeState * i, isizeState));
            }
        }
    }

#endif
}
#endif
}









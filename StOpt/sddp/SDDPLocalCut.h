// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SDDPLOCALCUT_H
#define SDDPLOCALCUT_H
#include <tuple>
#include <Eigen/Dense>
#include "geners/BinaryFileArchive.hh"
#include "StOpt/regression/LocalRegression.h"
#include "StOpt/sddp/SDDPACut.h"
#include "StOpt/sddp/SDDPVisitedStates.h"
#include "StOpt/sddp/SDDPCutBase.h"

/** \file SDDPLocalCut.h
 *  \brief Create SDDP Cuts for Local  Regressor
 *
 *  \author Xavier Warin
 */

namespace StOpt
{
/// \class SDDPLocalCut SDDPLocalCut.h
/// Create SDDP cuts
/// the problem to be solve is at each stage is
/// \f{eqnarray*}{
///     Q_t(X_t,W_t) & =  & \min_{U_t} C_t^T U_t + \mathbb{E}( Q_{t+1}(X_{t+1},W_{t+1}) \\ 
///                  &    & X_{t+1} = E X_{t+1} + B(W_t) \\
///                  &    &    X_{min} \le X_{i+1} \le X_{max}
/// \f}
/// where $X_t$ is the state vector and  $W_t$ is a vector of uncertainty
class SDDPLocalCut : public SDDPCutBase
{
private :

    int m_date ; ///< date identifier
    std::shared_ptr<LocalRegression> m_regressor ; ///< regressor object
    std::vector< std::vector<  std::shared_ptr<SDDPACut> > > m_cuts; ///< For each mesh of conditional expectation , give a list of all cuts
    int m_sample ; ///< number of samples used for each particle


#ifdef USE_MPI

    /// \brief Permits to calculate the routing schedule
    ///  cuts per simulations are spread over processors : they are reorganized in order to ease conditional expectation on all processors available
    /// \param   p_nbLP                 Total number of LP to solve
    /// \param   p_states               All the states visited by forward sweep
    /// \param   p_tabSendToProcessor   gives LP to send to another processor
    /// \param   p_tabRecFromProcessor  gives LP to receive from another processor
    /// \param   p_stateByProc          gives the state vector resolved by current processor
    /// \return  number of LP solved by current processor
    void routingSchedule(const int &p_nbLP, const SDDPVisitedStates &p_states,
                         std::vector< std::array<int, 2> > &p_tabSendToProcessor,
                         std::vector< std::array<int, 2> > &p_tabRecFromProcessor,
                         std::array<int, 2>    &p_stateByProc) const ;

    /// \brief mpi communications
    /// \param    p_cutPerSim           cut solved by current processor
    /// \param    p_tabSendToProcessor   gives LP to send to another processor
    /// \param    p_tabRecFromProcessor  gives LP to receive from another processor
    /// \param    p_cutPerSimProc       gives cut  necessary for current processor to calculate conditional expectation
    void mpiExec(const Eigen::ArrayXXd   &p_cutPerSim,
                 std::vector< std::array<int, 2> > &p_tabSendToProcessor,
                 std::vector< std::array<int, 2> > &p_tabRecFromProcessor,
                 Eigen::ArrayXXd    &p_cutPerSimProc) const ;

    /// \brief After all processors have calculated some conditional expectation of the cuts, these cuts are shared between all processors
    /// \param p_localCut  Vector of conditional cuts calculated by the current processor / in output all the cuts of the problem
    /// \param p_meshCut for each conditional cut gives the mesh where it is located    (entry for the processors, output for all processors)
    void mpiExecCutRoutage(std::vector< std::shared_ptr<SDDPACut> > &p_localCut, std::vector< int > &p_meshCut);

#endif

public :

    /// \brief Default constructor
    SDDPLocalCut();

    /// \brief Constructor
    /// \param p_date date identifier
    /// \param p_sample  number of sample for expectation
    /// \param p_regressor regressor
    SDDPLocalCut(const int   &p_date, const int &p_sample, std::shared_ptr<LocalRegression> p_regressor);

    /// \brief Constructor (used in forward part)
    /// \param p_date date identifier
    /// \param p_regressor regressor
    SDDPLocalCut(const int   &p_date, std::shared_ptr<LocalRegression> p_regressor);


    /// \brief create a vector of (stocks, particle) for LP to solve
    /// \param   p_states   visited states object
    /// \return  a vector  giving the state, the particle used for the LP, the mesh number associated
    std::vector< std::tuple< std::shared_ptr<Eigen::ArrayXd>, int, int >  > createVectorStatesParticle(const SDDPVisitedStates &p_states) const  ;

    /// \brief create cuts using result of all  the LP solved and store them adding them to an archive
    /// \param p_cutPerSim      cuts per simulation
    ///         - first dimension 1 + size of state X
    ///         - second dimension if the number of simulations
    /// \param p_states             visited states object
    /// \param p_vectorOfLp         vector of LP corresponding to cuts associated to p_visitedStates    : for each member of p_vectorOfLp, m_sample are generated in  p_cutPerSim
    /// \param p_ar                 binary archive used to store additional cuts
    void createAndStoreCuts(const Eigen::ArrayXXd &p_cutPerSim, const SDDPVisitedStates &p_states,
                            const std::vector< std::tuple< std::shared_ptr<Eigen::ArrayXd>, int, int >  > &p_vectorOfLp,
                            const std::shared_ptr<gs::BinaryFileArchive>   &p_ar);

    /// \brief Load already calculated cuts
    /// \param p_ar   archive to load cuts
    void loadCuts(const std::shared_ptr<gs::BinaryFileArchive> &p_ar);

    /// \brief get back all cuts associated to a mesh
    inline const std::vector< std::shared_ptr< SDDPACut > >   &getCutsForAMesh(const int &p_mesh) const
    {
        return m_cuts[p_mesh];
    }

    /// \brief get back members
    ///@{
    inline std::shared_ptr<LocalRegression>  getRegressor() const
    {
        return m_regressor ;
    }
    inline const std::vector< std::vector<  std::shared_ptr<SDDPACut> > > &getCuts() const
    {
        return m_cuts;
    }
    inline int  getSample() const
    {
        return m_sample;
    }
    inline int getUncertaintyDimension() const
    {
        return m_regressor-> getDimension();
    }
    ///@}

    /// \brief get back all the cuts associated to a particle number (state size by the number of cuts)
    /// \param p_isim  particle number
    Eigen::ArrayXXd  getCutsAssociatedToTheParticle(int p_isim) const;
    /// \brief get back all the cuts to a given particle  (state size by the number of cuts)
    /// \param p_aParticle  a particle
    Eigen::ArrayXXd  getCutsAssociatedToAParticle(const Eigen::ArrayXd &p_aParticle) const;

};
}
#endif

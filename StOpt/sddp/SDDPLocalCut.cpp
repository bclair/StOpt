#ifdef USE_MPI
#include <boost/mpi.hpp>
#include <boost/version.hpp>
#include "StOpt/core/parallelism/all_gatherv.hpp"
#endif
#include <vector>
#include <tuple>
#include <boost/serialization/vector.hpp>
#include "boost/lexical_cast.hpp"
#include "geners/BinaryFileArchive.hh"
#include "geners/Record.hh"
#include "geners/Reference.hh"
#include "StOpt/sddp/SDDPLocalCut.h"
#include "StOpt/sddp/SDDPACutGeners.h"

using namespace Eigen ;
using namespace std;
using namespace gs;

namespace StOpt
{

SDDPLocalCut::SDDPLocalCut() {}

SDDPLocalCut::SDDPLocalCut(const int &p_date, const int &p_sample, shared_ptr< LocalRegression >  p_regressor): m_date(p_date), m_regressor(p_regressor),
    m_cuts(p_regressor->getNbMeshTotal()), m_sample(p_sample) {}

SDDPLocalCut::SDDPLocalCut(const int &p_date, shared_ptr< LocalRegression >  p_regressor): m_date(p_date), m_regressor(p_regressor),
    m_cuts(p_regressor->getNbMeshTotal()), m_sample(1) {}

#ifdef USE_MPI
void  SDDPLocalCut::routingSchedule(const int &p_nbLP, const SDDPVisitedStates &p_states,
                                    vector< array<int, 2> > &p_tabSendToProcessor,
                                    vector< array<int, 2> > &p_tabRecFromProcessor,
                                    array<int, 2>   &p_stateByProc) const
{
    boost::mpi::communicator world;
    vector< array< int, 2> >  lpBelongingToProc(world.size());
    int nsimPProc = (int)(p_nbLP / world.size());
    int nRest = p_nbLP % world.size();
    for (int ip = 0 ; ip < world.size(); ++ip)
    {
        if (ip < nRest)
        {
            lpBelongingToProc[ip][0] = ip * nsimPProc + ip;
            lpBelongingToProc[ip][1] = lpBelongingToProc[ip][0] + nsimPProc + 1;
        }
        else
        {
            lpBelongingToProc[ip][0] = ip * nsimPProc + nRest;
            lpBelongingToProc[ip][1] = lpBelongingToProc[ip][0] + nsimPProc;
        }
    }
    vector< array<int, 2> >  lpNeededByProc(world.size());
    int nbState = p_states.getStateSize();
    vector<int> nbPartPerState(nbState);
    for (int ist = 0 ; ist < nbState; ++ist)
        nbPartPerState[ist] =  m_regressor-> getSimulBelongingToCell()[ p_states.getMeshAssociatedToState(ist)]->size() * m_sample;

    int nbStateProc = (int)(nbState / world.size());
    int nRestState = nbState % world.size();
    // calculate the LP simulations number (first and last (excluded)) needed by each processor to calculate conditional expectation
    int lpCounter = 0 ;
    for (int  ip = 0 ; ip < world.size(); ++ip)
    {
        lpNeededByProc[ip][0] = lpCounter;
        // conditional expectation number
        int iStateMin ;
        int iStateMax ;
        if (ip < nRestState)
        {
            iStateMin = ip * nbStateProc + ip;
            iStateMax = iStateMin + nbStateProc + 1;
        }
        else
        {
            iStateMin = ip * nbStateProc + nRestState;
            iStateMax = iStateMin + nbStateProc;
        }
        // traduction in term of lp
        for (int iProc = iStateMin; iProc < iStateMax ; ++iProc)
        {
            lpCounter += nbPartPerState[iProc];
        }
        lpNeededByProc[ip][1] = lpCounter;
        if (ip == world.rank())
        {
            p_stateByProc[0] = iStateMin;
            p_stateByProc[1] = iStateMax ;
        }
    }

    /// Particule to send to another processor (processor number)(part min, part max +1)
    p_tabSendToProcessor.resize(world.size());
    for (int ip = 0 ; ip < world.size() ; ++ip)
    {
        size_t iPartMin = max(lpNeededByProc[ip][0], lpBelongingToProc[world.rank()][0]);
        size_t iPartMax  = min(lpNeededByProc[ip][1], lpBelongingToProc[world.rank()][1]);
        if (iPartMin < iPartMax)
        {
            // in itask processor referential
            p_tabSendToProcessor[ip][0] = iPartMin - lpBelongingToProc[world.rank()][0];
            p_tabSendToProcessor[ip][1] = iPartMax - lpBelongingToProc[world.rank()][0];
        }
        else
        {
            p_tabSendToProcessor[ip][0] = -1;
            p_tabSendToProcessor[ip][1] = -1;
        }
    }
    for (int ip = world.size() ; ip < world.size() ; ++ip)
    {
        p_tabSendToProcessor[ip][0] = -1;
        p_tabSendToProcessor[ip][1] = -1;
    }

    /// Particule to receive from another processor (processor number)(part min, part max +1)
    p_tabRecFromProcessor.resize(world.size());
    for (int ip = 0 ; ip < world.size() ; ++ip)
    {
        size_t iPartMin = max(lpNeededByProc[world.rank()][0], lpBelongingToProc[ip][0]);
        size_t iPartMax  = min(lpNeededByProc[world.rank()][1], lpBelongingToProc[ip][1]);
        if (iPartMin < iPartMax)
        {
            // in ip processor referential
            p_tabRecFromProcessor[ip][0] = iPartMin - lpNeededByProc[world.rank()][0];
            p_tabRecFromProcessor[ip][1] = iPartMax - lpNeededByProc[world.rank()][0];
        }
        else
        {
            p_tabRecFromProcessor[ip][0] = -1;
            p_tabRecFromProcessor[ip][1] = -1;
        }
    }
}

void SDDPLocalCut::mpiExec(const ArrayXXd   &p_cutPerSim,
                           vector< array<int, 2> > &p_tabSendToProcessor,
                           vector< array<int, 2> > &p_tabRecFromProcessor,
                           ArrayXXd    &p_cutPerSimProc) const
{
    boost::mpi::communicator world;
    // number of rosws
    int nbRows = p_cutPerSim.rows();
    // communication receive part
    vector<  boost::mpi::request > reqRec(world.size());
    int nbRec = 0 ;
    // communication send part
    for (int iproc = 0 ; iproc < world.size(); ++iproc)
    {
        // size of send
        int sizeRec = (p_tabRecFromProcessor[iproc][1] - p_tabRecFromProcessor[iproc][0]) * nbRows;
        if (iproc != world.rank())
        {
            if (sizeRec > 0)
            {
                // communication
                int imesg_iproc = 0;
                reqRec[nbRec++] = world.irecv(iproc, imesg_iproc++, p_cutPerSimProc.col(p_tabRecFromProcessor[iproc][0]).data(), sizeRec);
            }
        }
        else
        {
            // same processor
            if (p_tabRecFromProcessor[world.rank()][1] > p_tabRecFromProcessor[world.rank()][0])
            {
                int begBlockRec = p_tabRecFromProcessor[world.rank()][0];
                int sizeBlockRec = p_tabRecFromProcessor[world.rank()][1] - begBlockRec;
                int begBlockSend = p_tabSendToProcessor[world.rank()][0];
                int sizeBlockSend = p_tabSendToProcessor[world.rank()][1] - begBlockSend;
                assert(sizeBlockRec == sizeBlockSend);
                p_cutPerSimProc.block(0, begBlockRec, nbRows, sizeBlockRec)  =  p_cutPerSim.block(0, begBlockSend, nbRows, sizeBlockSend);
            }
        }
    }
    vector<   boost::mpi::request > reqSend(world.size());
    int nbSend = 0 ;
    // communication send part
    for (int iproc = 0 ; iproc < world.size(); ++iproc)
    {
        // size of send
        int sizeSend = (p_tabSendToProcessor[iproc][1] - p_tabSendToProcessor[iproc][0]) * nbRows;
        if ((sizeSend > 0) && (iproc != world.rank()))
        {
            int imesg_iproc = 0;
            reqSend[nbSend++] =  world.isend(iproc, imesg_iproc++, p_cutPerSim.col(p_tabSendToProcessor[iproc][0]).data(), sizeSend);
        }
    }
    boost::mpi::wait_all(reqRec.begin(), reqRec.begin() + nbRec);
    boost::mpi::wait_all(reqSend.begin(), reqSend.begin() + nbSend);
}

void SDDPLocalCut::mpiExecCutRoutage(vector< shared_ptr<SDDPACut> > &p_localCut, vector< int > &p_meshCut)
{
    boost::mpi::communicator world;
    int isizeCut  = 0;
    int iRowCut   = 0;
    int iColCut   = 0 ;
    if (world.rank() == 0)
    {
        isizeCut =  p_localCut[0]->getCut()->size();
        iRowCut =   p_localCut[0]->getCut()->rows();
        iColCut  =  p_localCut[0]->getCut()->cols();
    }
    broadcast(world, isizeCut, 0);
    broadcast(world, iRowCut, 0);
    broadcast(world, iColCut, 0);
    ArrayXd arrayToGather(p_localCut.size()*isizeCut);
    for (size_t iCut = 0; iCut < p_localCut.size(); ++iCut)
    {
        Map< const ArrayXd > tab(p_localCut[iCut]->getCut()->data(), isizeCut);
        arrayToGather.segment(iCut * isizeCut, isizeCut) = tab;
    }
    // global vector
    ArrayXd  allCuts ;
    all_gatherv(world, arrayToGather.data(), arrayToGather.size(), allCuts);
    vector<int> allMesh;
    all_gatherv(world, p_meshCut.data(), p_meshCut.size(), allMesh);
    // create the cuts
    p_meshCut = allMesh;
    int nbTotCuts = allCuts.size() / isizeCut;
    p_localCut.resize(nbTotCuts);
    for (int i = 0; i < nbTotCuts; ++i)
    {
        shared_ptr<ArrayXXd > ptCut = make_shared< ArrayXXd>(iRowCut, iColCut);
        Map<ArrayXXd> mapCut(allCuts.data() + i * isizeCut, iRowCut, iColCut);
        *ptCut = mapCut;
        p_localCut[i] = make_shared< SDDPACut>(ptCut);
    }
}
#endif

vector< tuple< shared_ptr<ArrayXd>, int, int >  > SDDPLocalCut::createVectorStatesParticle(const SDDPVisitedStates &p_states) const
{
    vector< tuple< shared_ptr<ArrayXd>, int, int >  >  vectorOfLp;
    if (m_regressor->getNbSimul() == 0)
    {
        // just copy all stocks
        vectorOfLp.reserve(p_states.getStateSize());
        for (int istate  = 0; istate < p_states.getStateSize(); ++istate)
            vectorOfLp.push_back(make_tuple(p_states.getAState(istate), 0, 0));
    }
    else
    {
        // a cut is only affected to some particles
        int nbLP = 0;
        for (int istate  = 0; istate < p_states.getStateSize(); ++istate)
            nbLP += m_regressor->getSimulBelongingToCell()[p_states.getMeshAssociatedToState(istate)]->size();
        vectorOfLp.reserve(nbLP);
        for (int istate  = 0; istate < p_states.getStateSize(); ++istate)
        {
            int imesh = p_states.getMeshAssociatedToState(istate);
            for (size_t ipart  = 0; ipart < m_regressor->getSimulBelongingToCell()[imesh]->size() ; ++ipart)
                vectorOfLp.push_back(make_tuple(p_states.getAState(istate), (*m_regressor->getSimulBelongingToCell()[imesh])[ipart], imesh));
        }
    }
    return vectorOfLp ;
}

#ifdef USE_MPI
void SDDPLocalCut::createAndStoreCuts(const ArrayXXd &p_cutPerSim, const SDDPVisitedStates &p_states, const vector< tuple< shared_ptr<ArrayXd>, int, int >  > &p_vectorOfLp,
                                      const shared_ptr<BinaryFileArchive> &p_ar)
#else
void SDDPLocalCut::createAndStoreCuts(const ArrayXXd &p_cutPerSim, const SDDPVisitedStates &p_states, const vector< tuple< shared_ptr<ArrayXd>, int, int >  > &,
                                      const shared_ptr<BinaryFileArchive> &p_ar)
#endif
{
#ifdef USE_MPI
    // Tab to store to send to processor
    vector< array<int, 2> > tabSendToProcessor;
    // Tab to store what is to receive from other processor
    vector< array<int, 2> > tabRecFromProcessor;
    // first and last stock by current  processor
    array<int, 2>   stockByProc;
    routingSchedule(p_vectorOfLp.size()*m_sample, p_states, tabSendToProcessor, tabRecFromProcessor, stockByProc);
    int iFirstState = stockByProc[0];
    int iLastState = stockByProc[1] ;
    ArrayXXd cutPerSimProc(p_cutPerSim.rows(), p_vectorOfLp.size()*m_sample);
    mpiExec(p_cutPerSim, tabSendToProcessor, tabRecFromProcessor, cutPerSimProc);
#else
    int iFirstState = 0;
    int iLastState =   p_states.getStateSize() ;
    Map< const ArrayXXd > cutPerSimProc(p_cutPerSim.data(), p_cutPerSim.rows(), p_cutPerSim.cols());
#endif

    vector< shared_ptr<SDDPACut> > localCut;
    localCut.reserve(iLastState - iFirstState);
    vector<int> meshCut;
    meshCut.reserve(iLastState - iFirstState);
    int iposCut = 0;
    for (int isto = iFirstState; isto < iLastState; ++isto)
    {
        int imesh = p_states.getMeshAssociatedToState(isto); // mesh used for conditional expectation
        ArrayXXd cutExpectancy = ArrayXXd::Zero(p_cutPerSim.rows(), m_regressor->getSimulBelongingToCell()[imesh]->size());
        for (size_t is = 0; is < m_regressor->getSimulBelongingToCell()[imesh]->size(); ++is)
        {
            // conditional expectation with respect to state
            for (int isample = 0; isample < m_sample; ++isample)
            {
                cutExpectancy.col(is) += cutPerSimProc.col(iposCut++);

            }
            cutExpectancy.col(is) /= m_sample;
        }
        // now conditional expectation with respect to external  : create the cut
        shared_ptr<ArrayXXd> cutArray = make_shared<ArrayXXd>(m_regressor->getCoordBasisFunctionMultipleOneCell(imesh, cutExpectancy));
        shared_ptr<SDDPACut> cutToAdd = make_shared<SDDPACut>(cutArray);
        localCut.push_back(cutToAdd);
        meshCut.push_back(imesh);
    }

#if USE_MPI
    mpiExecCutRoutage(localCut, meshCut);
#endif
    vector<int> nbCutPerMesh(m_regressor->getNbMeshTotal(), 0);
    for (size_t i = 0; i < meshCut.size(); ++i)
        nbCutPerMesh[meshCut[i]] += 1;
    // add  cuts
    vector< vector<  shared_ptr<SDDPACut> > > additionalCuts(nbCutPerMesh.size());
    for (int imesh = 0; imesh < m_regressor->getNbMeshTotal(); ++imesh)
        additionalCuts[imesh].reserve(nbCutPerMesh[imesh]);
    for (size_t i = 0; i < meshCut.size(); ++i)
    {
        additionalCuts[meshCut[i]].push_back(localCut[i]);
    }
    // now store additional cuts
#if USE_MPI
    boost::mpi::communicator world;
    int itask = world.rank();

#else
    int itask = 0;
#endif
    if (itask == 0)
    {
        string stringStep = boost::lexical_cast<string>(m_date);
        for (size_t i = 0; i < additionalCuts.size(); ++i)
        {
            if (additionalCuts[i].size() > 0)
            {
                string stringCutMesh = "CutMesh" + boost::lexical_cast<string>(i);
                for (size_t j = 0; j < additionalCuts[i].size(); ++j)
                {
                    *p_ar << Record(*additionalCuts[i][j], stringCutMesh, stringStep);
                }
            }
        }
    }
    // add additional cuts to cuts already present for next (backward) time step
    for (size_t i = 0; i < additionalCuts.size(); ++i)
    {
        if (additionalCuts[i].size() > 0)
        {
            int isize = m_cuts[i].size();
            m_cuts[i].resize(isize + additionalCuts[i].size());
            for (size_t j = 0; j < additionalCuts[i].size(); ++j)
            {
                m_cuts[i][isize + j] = additionalCuts[i][j];
            }
        }
    }
}


void SDDPLocalCut::loadCuts(const shared_ptr< BinaryFileArchive>   &p_ar)
{
#if USE_MPI
    boost::mpi::communicator world;
    int itask = world.rank();
    if (itask == 0)
    {
#endif
        string stringStep = boost::lexical_cast<string>(m_date);
        m_cuts.resize(m_regressor->getNbMeshTotal());
        for (int i = 0; i < m_regressor->getNbMeshTotal(); ++i)
        {
            string stringCutMesh = "CutMesh" + boost::lexical_cast<string>(i);
            // number of cuts already generated
            gs::Reference< SDDPACut > refCut(*p_ar, stringCutMesh, stringStep);
            m_cuts[i].resize(refCut.size());
            for (size_t j = 0; j < refCut.size(); ++j)
                m_cuts[i][j] = refCut.getShared(j);
        }

#if USE_MPI
    }
    // use mpi to spread cuts
#if BOOST_VERSION <  105600
    boost::mpi::broadcast(world, m_cuts, 0);
#else
    // boost bug ?
    vector<int> ivec;
    vector< ArrayXXd> tabSerial;
    if (itask == 0)
    {
        ivec.resize(m_cuts.size());
        int itotal = 0 ;
        for (size_t i = 0; i < ivec.size(); ++i)
        {
            ivec[i] = m_cuts[i].size();
            itotal += m_cuts[i].size();
        }
        tabSerial.reserve(itotal);
        for (size_t i = 0; i < ivec.size(); ++i)
            for (int j = 0 ; j < ivec[i]; ++j)
                tabSerial.push_back(*m_cuts[i][j]->getCut());
    }
    boost::mpi::broadcast(world, ivec, 0);
    boost::mpi::broadcast(world, tabSerial, 0);
    if (itask > 0)
    {
        m_cuts.resize(ivec.size());
        int ipos = 0;
        for (size_t i = 0; i < ivec.size(); ++i)
        {
            m_cuts[i].resize(ivec[i]);
            for (int j = 0 ; j < ivec[i]; ++j)
            {
                shared_ptr<ArrayXXd> ptrTab = make_shared<ArrayXXd>(ArrayXXd(tabSerial[ipos++]));
                m_cuts[i][j] = make_shared<SDDPACut>(SDDPACut(ptrTab));
            }
        }
    }

#endif

#endif

}

ArrayXXd    SDDPLocalCut::getCutsAssociatedToTheParticle(int p_isim) const
{
    // cell associated
    int ncell = m_regressor->getCellAssociatedToSim(p_isim);
    if (m_cuts[ncell].size() == 0)
        return ArrayXXd();
    ArrayXd aParticule = ((m_regressor->getNbSimul() > 0) ? m_regressor->getParticle(p_isim) : ArrayXd());
    int iStateSize = m_cuts[ncell][0]->getStateSize();
    ArrayXXd retCut(iStateSize + 1, m_cuts[ncell].size());
    for (size_t icut = 0; icut < m_cuts[ncell].size(); ++icut)
    {
        retCut.col(icut) = m_regressor->getValuesOneCell(aParticule, ncell, *m_cuts[ncell][icut]->getCut());
    }
    return retCut;
}

ArrayXXd    SDDPLocalCut::getCutsAssociatedToAParticle(const ArrayXd &p_aParticle) const
{
    // cell associated
    int ncell = m_regressor->getMeshNumberAssociatedTo(p_aParticle);
    if (m_cuts[ncell].size() == 0)
        return ArrayXXd();
    int iStateSize = m_cuts[ncell][0]->getStateSize();
    ArrayXXd retCut(iStateSize + 1, m_cuts[ncell].size());
    for (size_t icut = 0; icut < m_cuts[ncell].size(); ++icut)
    {
        retCut.col(icut) = m_regressor->getValuesOneCell(p_aParticle, ncell, *m_cuts[ncell][icut]->getCut());
    }
    return retCut;
}


}

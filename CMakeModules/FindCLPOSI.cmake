# modeled after FindCOIN.cmake in the lemon project

# Written by: Xavier Warin
# Last updated: 02/12/2015

# This cmake file is designed to locate coin-related
# dependencies on a filesystem. 
#
# If the coin dependencies were installed in a non-standard
# directory, e.g. installed from source perhaps, then
# the user can provide a prefix hint via the CLPOSI_ROOT_DIR
# cmake variable:
#       $> cmake ../src -DCLPOSI_ROOT_DIR=/path/to/coin/root

# To date, this install requires the following dev versions
# of the respective coin libraries:
##       * coinor-libClp-dev
#       * coinor-libcoinutils-dev
#       * coinor-libOsi-dev

#
# Get the root directory hint if provided
#
IF(NOT DEFINED CLPOSI_ROOT_DIR)
    SET(CLPOSI_ROOT_DIR "$ENV{CLPOSI_ROOT_DIR}")
    MESSAGE("\tCLPOSI Root Dir: ${CLPOSI_INCLUDE_DIR}")
ENDIF(NOT DEFINED CLPOSI_ROOT_DIR)
MESSAGE(STATUS "CLPOSI_ROOT_DIR hint is : ${CLPOSI_ROOT_DIR}")

#
# Find the path based on a required header file
#
MESSAGE(STATUS "Coin multiple library dependency status:")
FIND_PATH(CLPOSI_INCLUDE_DIR coin/ClpModel.hpp
  HINTS "${CLPOSI_INCLUDE_DIR}"
  HINTS "${CLPOSI_ROOT_DIR}/include"
  HINTS /usr/
  HINTS /usr/include/
  HINTS /usr/local/
  HINTS /usr/local/include/
  HINTS /usr/coin/
  HINTS /usr/local/coin/
)
set(CLPOSI_INCLUDE_DIR ${CLPOSI_INCLUDE_DIR}/coin)
MESSAGE("\tCLPOSI Include Dir: ${CLPOSI_INCLUDE_DIR}")

#
# Find all coin library dependencies
#
FIND_LIBRARY(CLPOSI_CLP_LIBRARY
  NAMES Clp libClp #libClp.so.0
  HINTS ${CLPOSI_INCLUDE_DIR}/../../lib/
  HINTS "${CLPOSI_ROOT_DIR}/lib"
)
MESSAGE("\tCLPOSI CLP: ${CLPOSI_CLP_LIBRARY}")

FIND_LIBRARY(CLPOSI_UTILS_LIBRARY
  NAMES CoinUtils libCoinUtils #libCoinUtils.so.0
  HINTS ${CLPOSI_INCLUDE_DIR}/../../lib/
  HINTS "${CLPOSI_ROOT_DIR}/lib"
)
MESSAGE("\tCLPOSI UTILS: ${CLPOSI_CLPOSI_UTILS_LIBRARY}")

FIND_LIBRARY(CLPOSI_OSI_LIBRARY
  NAMES  Osi libOsi #libOsiClp.so.0
  HINTS ${CLPOSI_INCLUDE_DIR}/../../lib/
  HINTS "${CLPOSI_ROOT_DIR}/lib"
)
MESSAGE("\tCLPOSI OSI CLP: ${CLPOSI_OSI_CLP_LIBRARY}")


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CLPOSI DEFAULT_MSG
  CLPOSI_INCLUDE_DIR
  CLPOSI_CLP_LIBRARY
  CLPOSI_OSI_LIBRARY
)

#
# Set all required cmake variables based on our findings
#
IF(CLPOSI_FOUND)
  SET(CLPOSI_INCLUDE_DIRS ${CLPOSI_INCLUDE_DIR})
  SET(CLPOSI_LIBRARIES "${CLPOSI_UTILS_LIBRARY};${CLPOSI_CLP_LIBRARY};${CLPOSI_OSI_LIBRARY}")
ENDIF(CLPOSI_FOUND)

#
# Report a synopsis of our findings
#
IF(CLPOSI_INCLUDE_DIRS)
  MESSAGE(STATUS "Found CLPOSI Include Dirs: ${CLPOSI_INCLUDE_DIRS}")
ELSE(CLPOSI_INCLUDE_DIRS)
  MESSAGE(STATUS "CLPOSI Include Dirs NOT FOUND")
ENDIF(CLPOSI_INCLUDE_DIRS)

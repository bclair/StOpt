# modeled after FindCOIN.cmake in the lemon project

# Written by: Xavier Warin
# Last updated: 02/12/2015

# This cmake file is designed to locate coin-related
# dependencies on a filesystem. 
#
# If the coin dependencies were installed in a non-standard
# directory, e.g. installed from source perhaps, then
# the user can provide a prefix hint via the CLPOSI_ROOT_DIR
# cmake variable:
#       $> cmake ../src -DCLPOSI_ROOT_DIR=/path/to/coin/root

# To date, this install requires the following dev versions
# of the respective coin libraries:
##       * coinor-libClp-dev
#       * coinor-libcoinutils-dev
#       * coinor-libOsi-dev

#
# Get the root directory hint if provided
#
IF(NOT DEFINED COIN_ROOT_DIR)
    SET(COIN_ROOT_DIR "$ENV{COIN_ROOT_DIR}")
    MESSAGE("\tCOIN Root Dir: ${COIN_INCLUDE_DIR}")
ENDIF(NOT DEFINED COIN_ROOT_DIR)
MESSAGE(STATUS "COIN_ROOT_DIR hint is : ${COIN_ROOT_DIR}")

#
# Find the path based on a required header file 
#
MESSAGE(STATUS "Coin CLP multiple library dependency status:")
FIND_PATH(COIN_CLP_INCLUDE_DIR coin/ClpModel.hpp
  HINTS "${COIN_CLP_INCLUDE_DIR}"
  HINTS "${COIN_ROOT_DIR}/include"
  HINTS /usr/
  HINTS /usr/include/
  HINTS /usr/local/
  HINTS /usr/local/include/
  HINTS /usr/coin/
  HINTS /usr/local/coin/
)
set(COIN_CLP_INCLUDE_DIR ${COIN_CLP_INCLUDE_DIR}/coin)
MESSAGE("\tCLP Include Dir: ${COIN_CLP_INCLUDE_DIR}")

MESSAGE(STATUS "Coin UTILS multiple library dependency status:")
FIND_PATH(COIN_UTILS_INCLUDE_DIR coin/CoinModel.hpp
  HINTS "${COIN_UTILS_INCLUDE_DIR}"
  HINTS "${COIN_ROOT_DIR}/include"
  HINTS /usr/
  HINTS /usr/include/
  HINTS /usr/local/
  HINTS /usr/local/include/
  HINTS /usr/coin/
  HINTS /usr/local/coin/
)
set(COIN_UTILS_INCLUDE_DIR ${COIN_UTILS_INCLUDE_DIR}/coin)
MESSAGE("\tUTILS Include Dir: ${COIN_UTILS_INCLUDE_DIR}")

MESSAGE(STATUS "Coin OSI multiple library dependency status:")
FIND_PATH(COIN_OSI_INCLUDE_DIR coin/OsiCuts.hpp
  HINTS "${COIN_OSI_INCLUDE_DIR}"
  HINTS "${COIN_ROOT_DIR}/include"
  HINTS /usr/
  HINTS /usr/include/
  HINTS /usr/local/
  HINTS /usr/local/include/
  HINTS /usr/coin/
  HINTS /usr/local/coin/
)
set(COIN_OSI_INCLUDE_DIR ${COIN_OSI_INCLUDE_DIR}/coin)
MESSAGE("\tOSI Include Dir: ${COIN_OSI_INCLUDE_DIR}")

#
# Find all coin library dependencies
#
FIND_LIBRARY(COIN_CLP_LIBRARY
  NAMES Clp libClp #libClp.so.0
  HINTS ${COIN_CLP_INCLUDE_DIR}/../../lib/
  HINTS "${COIN_ROOT_DIR}/lib"
)
MESSAGE("\tCOIN CLP: ${COIN_CLP_LIBRARY}")

FIND_LIBRARY(COIN_UTILS_LIBRARY
  NAMES CoinUtils libCoinUtils #libCoinUtils.so.0
  HINTS ${COIN_UTILS_INCLUDE_DIR}/../../lib/
  HINTS "${COIN_ROOT_DIR}/lib"
)
MESSAGE("\tCOIN UTILS: ${COIN_UTILS_LIBRARY}")

FIND_LIBRARY(COIN_OSI_LIBRARY
  NAMES  Osi libOsi #libOsiClp.so.0
  HINTS ${COIN_OSI_INCLUDE_DIR}/../../lib/
  HINTS "${COIN_ROOT_DIR}/lib"
)
MESSAGE("\tCOIN OSI CLP: ${COIN_OSI_LIBRARY}")


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(COIN DEFAULT_MSG
  COIN_CLP_INCLUDE_DIR
  COIN_UTILS_INCLUDE_DIR
  COIN_OSI_INCLUDE_DIR
  COIN_CLP_LIBRARY
  COIN_OSI_LIBRARY
  COIN_UTILS_LIBRARY
)

#
# Set all required cmake variables based on our findings
#
IF(COIN_FOUND)
  SET(COIN_INCLUDE_DIRS "${COIN_CLP_INCLUDE_DIR};${COIN_UTILS_INCLUDE_DIR};${COIN_OSI_INCLUDE_DIR}")
  SET(COIN_LIBRARIES "${COIN_UTILS_LIBRARY};${COIN_CLP_LIBRARY};${COIN_OSI_LIBRARY}")
ENDIF(COIN_FOUND)

#
# Report a synopsis of our findings
#
IF(COIN_INCLUDE_DIRS)
  MESSAGE(STATUS "Found COIN Include Dirs: ${COIN_INCLUDE_DIRS}")
  MESSAGE(STATUS "Found COIN libs : ${COIN_LIBRARIES}")
ELSE(COIN_INCLUDE_DIRS)
  MESSAGE(STATUS "COIN Include Dirs NOT FOUND")
ENDIF(COIN_INCLUDE_DIRS)

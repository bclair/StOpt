// Copyright (C) 2018 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include "test/c++/tools/simulators/SimulatorGaussianSDDP.h"

/** \file Pybind11SDDPSimulators.cpp
 * \brief Map Simulators for SDDP
 * \author Xavier Warin
 */


namespace py = pybind11;


/// \brief Encapsulation for simulators
PYBIND11_MODULE(SDDPSimulators, m)
{
    py::class_<SimulatorGaussianSDDP, std::shared_ptr<SimulatorGaussianSDDP>, StOpt::SimulatorSDDPBase  >(m, "SimulatorGaussianSDDP")
    .def(py::init< const int &, const int &>())
    .def(py::init< const int &>())
    .def("updateDates", &SimulatorGaussianSDDP::updateDates)
    ;

}

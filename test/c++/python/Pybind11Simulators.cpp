// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include "StOpt/dp/SimulatorDPBase.h"
#include "test/c++/python/BlackScholesSimulatorWrap.h"
#include "test/c++/python/MeanRevertingWrap.h"
#include "test/c++/python/AR1Wrap.h"


namespace py = pybind11;

/// \brief Encapsulation for simulators
PYBIND11_MODULE(Simulators, m)
{

    ///  to map the constructor , should map  std::shared_ptr with boost python
    py::class_<BlackScholesSimulatorWrap,  std::shared_ptr< BlackScholesSimulatorWrap>, StOpt::SimulatorDPBase >(m, "BlackScholesSimulator")
    .def(py::init<   const Eigen::VectorXd &, const Eigen::VectorXd &, const Eigen::VectorXd &,  const Eigen::MatrixXd &, const double &,  const size_t &, const size_t &,  const bool &   >())
    .def("getMu", &BlackScholesSimulatorWrap::getMu)
    ;


    py::class_<MeanRevertingWrap, std::shared_ptr< MeanRevertingWrap>, StOpt::SimulatorDPBase  > (m, "MeanRevertingSimulator")
    .def(py::init< const FutureCurve &, const Eigen::VectorXd &,   const Eigen::VectorXd &,
         const double &, const double &,   const size_t &,  const size_t &, const bool & >())
    ;

    py::class_<AR1Wrap, std::shared_ptr< AR1Wrap>, StOpt::SimulatorDPBase  > (m, "AR1Simulator")
    .def(py::init< const double, const double, const double &,   const double &, const double &,    const size_t &,  const size_t &, const bool & >())
    ;

}

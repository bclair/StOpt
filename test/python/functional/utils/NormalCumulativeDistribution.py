# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import scipy as scp


# Calculate {1 \over { \sqrt{2 \pi}}}  \int_{\infty}^d e^{-x^2/2} dx
class NormalCumulativeDistribution :
    
    # constructor
    def __init__(self) :
        
        return None

    # calculate the value function
    # p_x point where the function is evaluated
    # return function value
    def operator(self, p_x) :
        
        p = 0.2316419
        b1 = 0.319381530
        b2 = -0.356563782
        b3 = 1.781477937
        b4 = -1.821255978
        b5 = 1.330274429

        if p_x > 0. :
            t = 1. / (1. + p * p_x)
            return 1. - scp.exp(-p_x * p_x / 2.) * t * (b1 + t * (b2 + t * (b3 + t * (b4 + t * b5)))) / (scp.sqrt(2. * scp.pi))
        elif p_x < 0. :
            t = 1. / (1. - p * p_x)
            return scp.exp(-p_x * p_x / 2.) * t * (b1 + t * (b2 + t * (b3 + t * (b4 + t * b5)))) / (scp.sqrt(2. * scp.pi))
        else :
            return 0.5

# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np


class FinalStepRegressionDP:
    
    def __init__(self, p_pGridCurrent, p_nbRegime):
        
        self.m_pGridCurrent = p_pGridCurrent
        self.m_nDim = p_pGridCurrent.getDimension()
        self.m_nbRegime = p_nbRegime
        
    def operator(self, p_funcValue, p_particles):
        
        finalValues = list(range(self.m_nbRegime))
        
        if self.m_pGridCurrent.getNbPoints() > 0:
            
            for iReg in range(self.m_nbRegime):
                finalValues[iReg] = np.zeros((p_particles.shape[1], self.m_pGridCurrent.getNbPoints()))
                
            # number of thread
            nbThreads = 1
            
            # create iterator on current grid treated for processor
            iterGridPoint = self.m_pGridCurrent.getGridIteratorInc(0)
            
            # iterates on points of the grid
            for iIter in range(self.m_pGridCurrent.getNbPoints()):
                iThread = 0
                
                if iterGridPoint.isValid():
                    pointCoord = iterGridPoint.getCoordinate()
                    
                    for iReg in range(self.m_nbRegime):
                        
                        #for i in range(p_particles.shape[1]):
                        finalValues[iReg][:,iterGridPoint.getCount()] = p_funcValue.set(iReg, pointCoord, p_particles)
                            
                    iterGridPoint.nextInc(nbThreads)
                    
        return finalValues

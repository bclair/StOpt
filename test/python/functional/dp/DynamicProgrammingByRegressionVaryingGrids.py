# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import dp.TransitionStepRegressionDP as trans
import dp.FinalStepRegressionDP as final


def DynamicProgrammingByRegression(p_timeChangeGrid, p_grids, p_optimize, p_regressor, p_funcFinalValue, p_pointStock, p_initialRegime, p_fileToDump) :
    
    # from the optimizer get back the simulation
    simulator = p_optimize.getSimulator()
    currentTime = simulator.getCurrentStep()
    iTime = len(p_timeChangeGrid) - 1
    
    while currentTime < p_timeChangeGrid[iTime] - 1e3 * 2.220446049250313e-16:
        iTime -= 1
        
    gridCurrent = p_grids[iTime]
    # final values
    valuesNext = final.FinalStepRegressionDP(gridCurrent, p_optimize.getNbRegime()).operator(p_funcFinalValue, simulator.getParticles())
    gridPrevious = gridCurrent
    
    # iterate on time steps
    for iStep in range(simulator.getNbStep()) :
        asset = simulator.stepBackwardAndGetParticles()
        currentTime = simulator.getCurrentStep()
        
        while currentTime < p_timeChangeGrid[iTime] - 1e3 * 2.220446049250313e-16:
            iTime -= 1
            
        gridCurrent = p_grids[iTime] 
        # conditional expectation operator
        if iStep == (simulator.getNbStep() - 1):
            p_regressor.updateSimulations(True, asset)
        else:
            p_regressor.updateSimulations(False, asset)
        
        # transition object
        transStep = trans.TransitionStepRegressionDP(gridCurrent, gridPrevious, p_optimize)
        valuesAndControl = transStep.oneStep(valuesNext, p_regressor)
        valuesNext = valuesAndControl[0]
        gridPrevious = gridCurrent
        
    # interpolate at the initial stock point and initial regime
    return (gridPrevious.createInterpolator(p_pointStock).applyVec(valuesNext[p_initialRegime])).mean()
